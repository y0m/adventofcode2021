use std::io::{self, Read};

mod scanner;

mod part1;
mod part2;

fn main() {
    let input = get_input();
    part1::part1(&input);
    part2::part2(&input);
}

fn get_input() -> String {
    let mut input = String::new();
    io::stdin().lock().read_to_string(&mut input).unwrap();
    return input.trim().to_string();
}
