use std::{
    collections::HashSet,
    ops::{Add, Neg, Sub},
};

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Vec3 {
    pub x: i32,
    pub y: i32,
    pub z: i32,
}

impl Add for Vec3 {
    type Output = Self;
    fn add(self, rhs: Self) -> Self::Output {
        Vec3 {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
            z: self.z + rhs.z,
        }
    }
}

impl Sub for Vec3 {
    type Output = Self;
    fn sub(self, rhs: Self) -> Self::Output {
        Vec3 {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
            z: self.z - rhs.z,
        }
    }
}

impl Neg for Vec3 {
    type Output = Self;
    fn neg(self) -> Self::Output {
        Self {
            x: -self.x,
            y: -self.y,
            z: -self.z,
        }
    }
}

impl Vec3 {
    pub fn zero() -> Self {
        Self { x: 0, y: 0, z: 0 }
    }

    pub fn rot(self, axis_rotations: Self) -> Self {
        let mut res = self;

        // x-axis rotation
        for _ in 0..axis_rotations.x {
            let prev_z = res.z;
            res.z = res.y;
            res.y = -prev_z;
        }

        // y-axis rotation
        for _ in 0..axis_rotations.y {
            let prev_z = res.z;
            res.z = -res.x;
            res.x = prev_z;
        }

        // z-axis rotation
        for _ in 0..axis_rotations.z {
            let prev_y = res.y;
            res.y = res.x;
            res.x = -prev_y;
        }

        res
    }
}

pub fn translate_points(src: &HashSet<Vec3>, translate: Vec3) -> HashSet<Vec3> {
    src.iter().map(|p| *p + translate).collect()
}

pub fn find_offset(a: &HashSet<Vec3>, b: &HashSet<Vec3>, min_similar: usize) -> Option<Vec3> {
    for a_origin in a.iter() {
        let a_translated = translate_points(a, -*a_origin);
        for b_origin in b.iter() {
            let b_translated = translate_points(b, -*b_origin);
            if a_translated.intersection(&b_translated).count() >= min_similar {
                return Some(*a_origin - *b_origin);
            }
        }
    }

    None
}

pub fn all_rotations() -> Vec<Vec3> {
    let mut res = Vec::new();
    let mut known_rots = HashSet::new();

    for xrot in 0..=3 {
        for yrot in 0..=3 {
            for zrot in 0..=3 {
                let rot = Vec3 {
                    x: xrot,
                    y: yrot,
                    z: zrot,
                };
                let axes_rotated = (
                    Vec3 { x: 1, y: 0, z: 0 }.rot(rot),
                    Vec3 { x: 0, y: 1, z: 0 }.rot(rot),
                    Vec3 { x: 0, y: 0, z: 1 }.rot(rot),
                );
                if known_rots.contains(&axes_rotated) {
                    continue;
                }

                known_rots.insert(axes_rotated);
                res.push(rot);
            }
        }
    }

    res
}
