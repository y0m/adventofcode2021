#![allow(dead_code)]
use std::env::args;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;
use std::time::Instant;

fn main() {
    let filename = args().nth(1).expect("program [filename]");
    if let Ok(lines) = read_lines(filename) {
        let depths = load_depths(lines);

        let now = Instant::now();
        let count = increases_part1(&depths);
        let elapsed = now.elapsed();
        eprintln!("{} ns", elapsed.as_nanos());
        println!("increases part1: {}", count);

        let now = Instant::now();
        let count = increases_part2(&depths);
        let elapsed = now.elapsed();
        eprintln!("{} ns", elapsed.as_nanos());
        println!("increases part2: {}", count);
    }
}

fn load_depths(lines: Vec<String>) -> Vec<usize> {
    lines.iter().map(|x| x.parse::<usize>().unwrap()).collect()
}

// Faster, less sexy to read
fn increases_part1(depths: &[usize]) -> usize {
    let mut count = 0;
    let mut prev: Option<usize> = None;
    for &depth in depths {
        if let Some(value) = prev {
            if depth > value {
                count += 1;
            }
        }
        prev = Some(depth);
    }
    count
}

const WINDOWSIZE: usize = 3;
fn increases_part2(depths: &[usize]) -> usize {
    let mut count = 0;
    let mut prev = None;
    for n in 0..=(depths.len() - WINDOWSIZE) {
        let wnd = &depths[n..(n + WINDOWSIZE)];
        let sum = wnd.iter().sum::<usize>();
        if let Some(prevsum) = prev {
            if sum > prevsum {
                count += 1;
            }
        }
        prev = Some(sum);
    }
    count
}

fn read_lines<P>(filename: P) -> io::Result<Vec<String>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sample_test_part1() {
        let mut value = 0usize;
        if let Ok(lines) = read_lines("./sample.txt") {
            let depths = load_depths(lines);
            value = increases_part1(&depths);
        }
        assert_eq!(value, 7);
    }

    #[test]
    fn sample_test_part2() {
        let mut value = 0usize;
        if let Ok(lines) = read_lines("./sample.txt") {
            let depths = load_depths(lines);
            value = increases_part2(&depths);
        }
        assert_eq!(value, 5);
    }
}
