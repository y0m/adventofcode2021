use std::time::Instant;

fn main() {
    let now = Instant::now();
    println!("{}", part1(6, 3));
    let elapsed = now.elapsed();
    eprintln!("{} ns", elapsed.as_nanos());

    let now = Instant::now();
    println!("{}", part2(6, 3));
    let elapsed = now.elapsed();
    eprintln!("{} ns", elapsed.as_nanos());
}

fn part1(sp1: usize, sp2: usize) -> usize {
    let mut p1 = 0;
    let mut sp1 = sp1;
    let mut p2 = 0;
    let mut sp2 = sp2;
    let mut n = 1;
    let looser = loop {
        turn_part1(&mut n, &mut sp1, 100, 10);
        p1 += sp1;
        if p1 >= 1000 {
            break p2;
        }
        turn_part1(&mut n, &mut sp2, 100, 10);
        p2 += sp2;
        if p1 >= 1000 {
            break p1;
        }
    };
    looser * (n - 1)
}

const DIRAC: [(usize, usize); 7] = [(3, 1), (4, 3), (5, 6), (6, 7), (7, 6), (8, 3), (9, 1)];
fn part2(sp1: usize, sp2: usize) -> usize {
    turn_part2(0, &[sp1, sp2], &[0, 0])
        .into_iter()
        .max()
        .unwrap()
}

fn turn_part2(p: usize, sp: &[usize], score: &[usize]) -> [usize; 2] {
    let mut out: Option<[usize; 2]> = None;
    if score[0] >= 21 {
        // p1 won, let's bottom up
        out = Some([1, 0]);
    }
    if score[1] >= 21 {
        // p2 won, let's bottom up
        out = Some([0, 1]);
    }
    if out.is_none() {
        out = Some([0, 0]);
        for (roll, freq) in DIRAC {
            let sp = &mut [sp[0], sp[1]];
            sp[p] = ((sp[p] + roll - 1) % 10) + 1;
            let score = &mut [score[0], score[1]];
            score[p] += sp[p];
            let res = turn_part2(1 - p, sp, score);
            let o = out.unwrap();
            out = Some([o[0] + res[0] * freq, o[1] + res[1] * freq]);
        }
    }
    out.unwrap()
}

fn turn_part1(n: &mut usize, sp: &mut usize, si: usize, sc: usize) {
    let mut sum = 0;
    for _ in 0..3 {
        sum += ((*n - 1) % si) + 1;
        *n += 1;
    }
    *sp = ((*sp + sum - 1) % sc) + 1;
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sample_part1() {
        assert_eq!(part1(4, 8), 739785);
    }

    #[test]
    fn sample_part2() {
        assert_eq!(part2(4, 8), 444356092776315);
    }
}
