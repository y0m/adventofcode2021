#![allow(dead_code)]
use std::collections::HashMap;
use std::env::args;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;
use std::time::Instant;

fn main() {
    let filename = args().nth(1).expect("program [filename]");
    if let Ok(lines) = read_lines(filename) {
        let polymer = load_polymer(&lines);
        let now = Instant::now();
        let diff = part1(&polymer, 10);
        let elapsed = now.elapsed();
        eprintln!("{} ns", elapsed.as_nanos());
        println!("{}", diff);
        let now = Instant::now();
        let diff = part1(&polymer, 40);
        let elapsed = now.elapsed();
        eprintln!("{} ns", elapsed.as_nanos());
        println!("{}", diff);
    }
}

fn part1(p: &Polymer, steps: usize) -> usize {
    // init pairs compute
    let mut keycount: HashMap<&[u8], usize> = HashMap::new();
    for key in p.pair.keys() {
        keycount.insert(key, 0);
    }
    for (pos, &c) in p.template.iter().take(p.template.len() - 1).enumerate() {
        let count = keycount
            .get_mut([c, p.template[pos + 1]].as_slice())
            .unwrap();
        *count += 1;
    }

    // compute pairs
    for _i in 1..=steps {
        let mut next = keycount.clone();

        for key in p.pair.keys() {
            let count = next.get_mut(&*p.pair[key][0]).unwrap();
            *count += keycount[key];
            let count = next.get_mut(&*p.pair[key][1]).unwrap();
            *count += keycount[key];
            let count = next.get_mut(key).unwrap();
            *count -= keycount[key];
        }
        keycount = next.clone();
    }

    // count pairs for characters
    let mut counts: HashMap<u8, usize> = HashMap::new();
    for key in p.pair.keys() {
        for c in *key {
            if let Some(value) = counts.get_mut(c) {
                *value += keycount[key];
            } else {
                counts.insert(*c, keycount[key]);
            }
        }
    }
    let count = counts.get_mut(&p.template[0]).unwrap();
    *count += 1;
    let count = counts.get_mut(&p.template[p.template.len() - 1]).unwrap();
    *count += 1;
    let max = counts.values().max().unwrap();
    let min = counts.values().min().unwrap();

    (max - min) / 2
}

struct Polymer<'a> {
    template: &'a [u8],
    pair: HashMap<&'a [u8], Vec<Vec<u8>>>,
}

fn load_polymer(lines: &[String]) -> Polymer {
    let mut p = Polymer {
        template: lines[0].as_bytes(),
        pair: HashMap::new(),
    };
    for (i, line) in lines.iter().enumerate() {
        if i > 1 {
            let mut pair = line.split(" -> ").map(|v| v.as_bytes());
            let first = pair.next().unwrap();
            let second = pair.next().unwrap();
            p.pair.insert(
                first,
                vec![vec![first[0], second[0]], vec![second[0], first[1]]],
            );
        }
    }
    p
}

fn read_lines<P>(filename: P) -> io::Result<Vec<String>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sample_part1() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample.txt") {
            let polymer = load_polymer(&lines);
            value = part1(&polymer, 10);
        }
        assert_eq!(value, 1588);
    }

    #[test]
    fn sample_part2() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample.txt") {
            let polymer = load_polymer(&lines);
            value = part1(&polymer, 40);
        }
        assert_eq!(value, 2188189693529);
    }
}
