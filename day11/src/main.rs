#![allow(dead_code)]
use std::env::args;
use std::fs::File;
use std::io::{self, BufRead, Result};
use std::path::Path;
use std::time::Instant;

fn main() {
    let filename = args().nth(1).expect("program [filename]");
    if let Ok(mut octopuses) = load_octo(filename) {
        let mut oz2 = octopuses.clone();
        let now = Instant::now();
        let flashes = octopuses.part1(100);
        let elapsed = now.elapsed();
        eprintln!("{} ns", elapsed.as_nanos());
        println!("{}", flashes);
        let now = Instant::now();
        let mut steps = 1;
        while !oz2.part2(1) {
            steps += 1;
        }
        let elapsed = now.elapsed();
        eprintln!("{} ns", elapsed.as_nanos());
        println!("{}", steps);
    }
}

#[derive(Clone)]
struct Octopuses {
    width: usize,
    octopus: Vec<(usize, bool)>,
    adjacents: Vec<Vec<usize>>,
}

impl Octopuses {
    fn part1(&mut self, steps: usize) -> usize {
        let mut flashes = 0;
        for _i in 1..=steps {
            self.octopus.iter_mut().for_each(|o| {
                (*o).0 += 1;
            });
            let prevflashes = flashes;
            loop {
                let onf = self
                    .octopus
                    .iter()
                    .enumerate()
                    .filter(|(_, o)| o.0 > 9 && !o.1)
                    .map(|(idx, _)| idx)
                    .collect::<Vec<_>>();
                if onf.is_empty() {
                    break;
                }
                onf.iter().for_each(|&idx| {
                    self.octopus.get_mut(idx).unwrap().1 = true;
                    flashes += 1;
                    self.adjacents[idx].iter().for_each(|&a| {
                        self.octopus.get_mut(a).unwrap().0 += 1;
                    });
                });
            }
            if prevflashes != flashes {
                self.octopus.iter_mut().filter(|f| f.1).for_each(|o| {
                    (*o).0 = 0;
                    (*o).1 = false;
                });
            }
        }
        flashes
    }

    fn part2(&mut self, steps: usize) -> bool {
        let mut flashes = 0;
        for _i in 1..=steps {
            self.octopus.iter_mut().for_each(|o| {
                (*o).0 += 1;
            });
            let prevflashes = flashes;
            loop {
                let onf = self
                    .octopus
                    .iter()
                    .enumerate()
                    .filter(|(_, o)| o.0 > 9 && !o.1)
                    .map(|(idx, _)| idx)
                    .collect::<Vec<_>>();
                if onf.is_empty() {
                    break;
                }
                onf.iter().for_each(|&idx| {
                    self.octopus.get_mut(idx).unwrap().1 = true;
                    flashes += 1;
                    self.adjacents[idx].iter().for_each(|&a| {
                        self.octopus.get_mut(a).unwrap().0 += 1;
                    });
                });
            }
            if prevflashes != flashes {
                self.octopus.iter_mut().filter(|f| f.1).for_each(|o| {
                    (*o).0 = 0;
                    (*o).1 = false;
                });
            }
            if self.octopus.iter().filter(|&o| o.0 == 0).count() == self.octopus.len() {
                return true;
            }
        }
        false
    }

    fn print(&self) {
        for (i, o) in self.octopus.iter().enumerate() {
            if i % self.width == 0 {
                println!();
            }
            print!("{}", o.0);
        }
        println!();
    }
}

fn load_octo<P>(filename: P) -> Result<Octopuses>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    let lines = io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect::<Vec<_>>();
    let bounds = (lines[0].len(), lines.len());
    let mut octopuses = Octopuses {
        width: bounds.0,
        octopus: vec![(0, false); bounds.0 * bounds.1],
        adjacents: vec![vec![]; bounds.0 * bounds.1],
    };
    for (y, line) in lines.iter().enumerate() {
        line.as_bytes().iter().enumerate().for_each(|(x, &b)| {
            let index = x + y * bounds.0;
            octopuses.octopus[index] = ((b - b'0') as usize, false);
            set_adjacents(&mut octopuses.adjacents, (x, y), bounds);
        });
    }
    Ok(octopuses)
}

fn set_adjacents(adjacents: &mut Vec<Vec<usize>>, coord: (usize, usize), bounds: (usize, usize)) {
    let top = coord.1 as i64 - 1;
    let left = coord.0 as i64 - 1;
    let down = coord.1 + 1;
    let right = coord.0 + 1;
    let index = coord.0 + coord.1 * bounds.0;
    if left >= 0 {
        if top >= 0 {
            adjacents[index].push(left as usize + top as usize * bounds.0);
        }
        adjacents[index].push(left as usize + coord.1 * bounds.0);
        if down < bounds.1 {
            adjacents[index].push(left as usize + down * bounds.0);
        }
    }
    if right < bounds.0 {
        if top >= 0 {
            adjacents[index].push(right as usize + top as usize * bounds.0);
        }
        adjacents[index].push(right as usize + coord.1 * bounds.0);
        if down < bounds.1 {
            adjacents[index].push(right as usize + down * bounds.0);
        }
    }
    if top >= 0 {
        adjacents[index].push(coord.0 + top as usize * bounds.0);
    }
    if down < bounds.1 {
        adjacents[index].push(coord.0 + down * bounds.0);
    }
    adjacents[index].sort_unstable();
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sample_part1() {
        let mut value = 0;
        if let Ok(mut octopuses) = load_octo("./sample.txt") {
            value += octopuses.part1(100);
            octopuses.print();
        }
        assert_eq!(value, 1656);
    }

    #[test]
    fn sample_part2() {
        let mut value = false;
        if let Ok(mut octopuses) = load_octo("./sample.txt") {
            value = octopuses.part2(195);
            octopuses.print();
        }
        assert_eq!(value, true);
    }

    #[test]
    fn sample1_2steps() {
        let mut value = 0;
        if let Ok(mut octopuses) = load_octo("./sample1.txt") {
            octopuses.print();
            value += octopuses.part1(1);
            octopuses.print();
            value += octopuses.part1(1);
            octopuses.print();
        }
        assert_eq!(value, 9);
    }
}
