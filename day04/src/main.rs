#![allow(dead_code)]
use std::collections::HashMap;
use std::env::args;
use std::fs::File;
use std::io::{self, BufRead, Result};
use std::path::Path;
use std::time::Instant;

fn main() {
    let filename = args().nth(1).expect("program [filename]");
    if let Ok(lines) = read_lines(filename) {
        if let Some((numbers, cards)) = load_bingo(&lines) {
            let now = Instant::now();
            let res = bingo_part1(numbers, cards);
            let elapsed = now.elapsed();
            eprintln!("{} ns", elapsed.as_nanos());
            println!("{}", res);
        }
        if let Some((numbers, cards)) = load_bingo(&lines) {
            let now = Instant::now();
            let res = bingo_part2(numbers, cards);
            let elapsed = now.elapsed();
            eprintln!("{} ns", elapsed.as_nanos());
            println!("{}", res);
        }
    }
}

fn read_lines<P>(filename: P) -> Result<Vec<String>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect())
}

fn bingo_part1(numbers: Vec<usize>, mut cards: Vec<Card>) -> usize {
    for num in numbers {
        for card in &mut cards {
            if let Some(full) = card.mark(num) {
                return num * full;
            }
        }
    }
    0
}

fn bingo_part2(numbers: Vec<usize>, mut cards: Vec<Card>) -> usize {
    let mut last = 0;
    let mut res: HashMap<usize, usize> = HashMap::new();
    for num in numbers {
        for (index, card) in cards.iter_mut().enumerate() {
            if let std::collections::hash_map::Entry::Vacant(e) = res.entry(index) {
                if let Some(full) = card.mark(num) {
                    last = full * num;
                    e.insert(last);
                }
            }
        }
    }
    last
}

fn load_bingo(lines: &[String]) -> Option<(Vec<usize>, Vec<Card>)> {
    if lines.is_empty() {
        return None;
    }
    let numbers = lines[0]
        .split(',')
        .map(|x| x.parse::<usize>().unwrap())
        .collect::<Vec<_>>();

    let mut cards: Vec<Card> = Vec::new();
    for line in &lines[1..] {
        if line.is_empty() {
            cards.push(Card::new());
        } else {
            let last = cards.len() - 1;
            line.split_whitespace()
                .map(|x| x.parse::<usize>().unwrap())
                .for_each(|x| cards[last].push(x));
        }
    }
    Some((numbers, cards))
}

const CARDWIDTH: usize = 5;
const CARDSIZE: usize = 25;
#[derive(Debug)]
struct NumState {
    marked: bool,
    index: usize,
}
#[derive(Debug)]
struct Card {
    inner: Vec<usize>,
    marks: HashMap<usize, NumState>,
}

impl Card {
    fn new() -> Self {
        let mut card = Card {
            inner: Vec::new(),
            marks: HashMap::new(),
        };
        card.inner.reserve(CARDSIZE);
        card
    }

    fn push(&mut self, num: usize) {
        let state = NumState {
            index: self.inner.len(),
            marked: false,
        };
        self.inner.push(num);
        self.marks.insert(num, state);
    }

    fn mark(&mut self, num: usize) -> Option<usize> {
        if let Some(state) = self.marks.get_mut(&num) {
            let row = state.index / CARDWIDTH;
            let col = state.index % CARDWIDTH;
            state.marked = true;
            if self.row_marked(row) || self.col_marked(col) {
                return Some(self.get_unmarked_sum());
            }
        }
        None
    }

    fn row_marked(&self, row: usize) -> bool {
        let mut mark = true;
        let from = row * CARDWIDTH;
        let to = (row + 1) * CARDWIDTH;
        for num in &self.inner[from..to] {
            let state = self.marks.get(num).unwrap();
            mark &= state.marked;
            if !mark {
                break;
            }
        }
        mark
    }

    fn col_marked(&self, col: usize) -> bool {
        let mut mark = true;
        let mut index = col;
        while index < CARDSIZE && mark {
            let num = self.inner[index];
            let state = self.marks.get(&num).unwrap();
            mark &= state.marked;
            index += CARDWIDTH;
        }
        mark
    }

    fn get_unmarked_sum(&self) -> usize {
        self.marks
            .iter()
            .filter(|(_, v)| !(*v).marked)
            .map(|(&k, _)| k)
            .sum()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sample_test_part1() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample.txt") {
            if let Some((numbers, cards)) = load_bingo(&lines) {
                value = bingo_part1(numbers, cards);
            }
        }
        assert_eq!(value, 4512);
    }

    #[test]
    fn sample_test_part2() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample.txt") {
            if let Some((numbers, cards)) = load_bingo(&lines) {
                value = bingo_part2(numbers, cards);
            }
        }
        assert_eq!(value, 1924);
    }
}
