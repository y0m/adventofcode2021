#![allow(dead_code)]
use std::{env::args, time::Instant};

fn main() {
    let config = args().nth(1).expect("program \"[input_string]\"");
    let target = get_target(&config);

    let now = Instant::now();
    let ymax = part1(&target);
    let elapsed = now.elapsed();
    eprintln!("{} ns", elapsed.as_nanos());
    println!("{}", ymax);

    let now = Instant::now();
    let count = part2(&target);
    let elapsed = now.elapsed();
    eprintln!("{} ns", elapsed.as_nanos());
    println!("{}", count);
}

fn part1(target: &[Vec<i64>]) -> i64 {
    suite_vx(target[1][0])
}

fn suite_vx(n: i64) -> i64 {
    (n.abs() * (n.abs() - 1)) / 2
}

fn part2(target: &[Vec<i64>]) -> usize {
    let mut count = 0;

    let vy_min = target[1][0];
    let vx_max = target[0][1];
    let vy_max = -target[1][0];
    let vx_min = ((2.0 * target[0][0] as f64).sqrt() - 1.0).floor() as i64;
    let upper_bound = (target[0][0], target[1][1]);
    let lower_bound = (target[0][1], target[1][0]);

    for vx in vx_min..=vx_max {
        for vy in vy_min..=vy_max {
            if will_intersect((vx, vy), upper_bound, lower_bound) {
                count += 1;
            }
        }
    }

    count
}

fn get_pos(vx0: i64, vy0: i64, t: i64) -> (i64, i64) {
    let y = vy0 * t - (t - 1) * (t) / 2;
    let x = if t < vx0 {
        (2 * vx0 - t + 1) * (t) / 2
    } else {
        (vx0 * (vx0 + 1) / 2) as i64
    };
    (x, y)
}

fn will_intersect(v: (i64, i64), u: (i64, i64), l: (i64, i64)) -> bool {
    let tmin = (((v.1 * v.1 - 2 * u.1) as f64).sqrt() + v.1 as f64).floor() as i64;
    let tmax = (((v.1 * v.1 - 2 * l.1) as f64).sqrt() + v.1 as f64).floor() as i64;

    for t in tmin..=tmax + 1 {
        let (x, y) = get_pos(v.0, v.1, t);
        if x >= u.0 && x <= l.0 && y >= l.1 && y <= u.1 {
            return true;
        }
    }

    false
}

fn get_target(config: &str) -> Vec<Vec<i64>> {
    let mut config = String::from(config);
    config = config
        .replace("target area: ", "")
        .replace("x=", "")
        .replace("y=", "");
    config
        .split(", ")
        .map(|s| {
            s.split("..")
                .map(|s| s.parse::<i64>().unwrap())
                .collect::<Vec<_>>()
        })
        .collect()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sample_part1() {
        let target = get_target("target area: x=20..30, y=-10..-5");
        assert_eq!(part1(&target), 45);
    }

    #[test]
    fn sample_part2() {
        let target = get_target("target area: x=20..30, y=-10..-5");
        assert_eq!(part2(&target), 112);
    }
}
