#![allow(dead_code)]
#![feature(slice_group_by)]
use std::env::args;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;
use std::time::Instant;

fn main() {
    let filename = args().nth(1).expect("program [filename]");
    if let Ok(lines) = read_lines(filename) {
        let graph = load_graph(&lines);

        let now = Instant::now();
        let count = part1(&graph);
        let elapsed = now.elapsed();
        eprintln!("{} ns", elapsed.as_nanos());
        println!("{}", count);

        let now = Instant::now();
        let count = part2(&graph);
        let elapsed = now.elapsed();
        eprintln!("{} ns", elapsed.as_nanos());
        println!("{}", count);
    }
}

fn part1(g: &Graph) -> usize {
    if let Some(st) = g.start {
        let mut paths = vec![vec![st]];
        build1(g, &mut paths, 0);

        return paths
            .iter()
            .filter(|&p| *p.iter().last().unwrap() == g.end.unwrap())
            .count();
    }
    0
}

fn part2(g: &Graph) -> usize {
    if let Some(st) = g.start {
        let mut paths = vec![vec![st]];
        build2(g, &mut paths, 0);

        return paths
            .iter()
            .filter(|&p| *p.iter().last().unwrap() == g.end.unwrap())
            .count();
    }
    0
}

fn build1(g: &Graph, paths: &mut Vec<Vec<usize>>, path: usize) {
    let len = paths[path].len();
    let nidx = paths[path][len - 1];

    let mut nodes = g
        .edges
        .iter()
        .filter(|&e| e.contains(&nidx))
        .flatten()
        .filter(|&&i| {
            i != nidx && i != g.start.unwrap() && !(g.nodes[i].small && paths[path].contains(&i))
        })
        .collect::<Vec<_>>();
    nodes.sort();
    nodes.dedup();

    let mut pos = path;
    let atpath = paths[path].clone();
    for &next in nodes {
        if paths.get(pos).is_none() {
            paths.push(atpath.clone());
        }
        paths[pos].push(next);
        if g.nodes[next].label != "end" {
            build1(g, paths, pos);
        }
        pos = paths.len()
    }
}

fn build2(g: &Graph, paths: &mut Vec<Vec<usize>>, path: usize) {
    let len = paths[path].len();
    let nidx = paths[path][len - 1];

    let mut visits = paths[path]
        .iter()
        .filter(|&&idx| g.nodes[idx].small)
        .collect::<Vec<_>>();
    visits.sort();
    let visit = visits
        .group_by(|&&a, &&b| a == b)
        .map(|x| x.len())
        .max()
        .unwrap();
    let visit = if visit < 2 { 2 } else { 1 };

    let mut nodes = g
        .edges
        .iter()
        .filter(|&e| e.contains(&nidx))
        .flatten()
        .filter(|&&i| {
            i != nidx
                && i != g.start.unwrap()
                && !(g.nodes[i].small && paths[path].iter().filter(|&&n| n == i).count() >= visit)
        })
        .collect::<Vec<_>>();
    nodes.sort();
    nodes.dedup();

    let mut pos = path;
    let atpath = paths[path].clone();
    for &next in nodes {
        if paths.get(pos).is_none() {
            paths.push(atpath.clone());
        }
        paths[pos].push(next);
        if g.nodes[next].label != "end" {
            build2(g, paths, pos);
        }
        pos = paths.len()
    }
}

#[derive(Debug, Clone, Copy)]
struct Cave<'a> {
    label: &'a str,
    small: bool,
}

#[derive(Debug)]
struct Graph<'a> {
    start: Option<usize>,
    end: Option<usize>,
    nodes: Vec<Cave<'a>>,
    edges: Vec<[usize; 2]>,
}

fn load_graph(lines: &[String]) -> Graph {
    let mut g = Graph {
        start: None,
        end: None,
        nodes: Vec::new(),
        edges: Vec::new(),
    };
    for line in lines {
        let nodes = line.split('-').collect::<Vec<_>>();
        // nodes
        let nidx1: usize;
        if let Some((n1, _)) = g
            .nodes
            .iter()
            .enumerate()
            .find(|(_, &n)| n.label == nodes[0])
        {
            nidx1 = n1;
        } else {
            nidx1 = g.nodes.len();
            g.nodes.push(Cave {
                label: nodes[0],
                small: nodes[0].chars().next().unwrap().is_lowercase(),
            });
            if nodes[0] == "start" && g.start.is_none() {
                g.start = Some(nidx1);
            }
            if nodes[0] == "end" && g.end.is_none() {
                g.end = Some(nidx1);
            }
        }
        let nidx2: usize;
        if let Some((n2, _)) = g
            .nodes
            .iter()
            .enumerate()
            .find(|(_, &n)| n.label == nodes[1])
        {
            nidx2 = n2;
        } else {
            nidx2 = g.nodes.len();
            g.nodes.push(Cave {
                label: nodes[1],
                small: nodes[1].chars().next().unwrap().is_lowercase(),
            });
            if nodes[1] == "start" && g.start.is_none() {
                g.start = Some(nidx2);
            }
            if nodes[1] == "end" && g.end.is_none() {
                g.end = Some(nidx2);
            }
        }
        // edges
        g.edges.push([nidx1, nidx2]);
    }
    g
}

fn read_lines<P>(filename: P) -> io::Result<Vec<String>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sample1_part1() {
        let mut value = 0usize;
        if let Ok(lines) = read_lines("./sample1.txt") {
            let graph = load_graph(&lines);
            value = part1(&graph);
        }
        assert_eq!(value, 10);
    }

    #[test]
    fn sample2_part1() {
        let mut value = 0usize;
        if let Ok(lines) = read_lines("./sample2.txt") {
            let graph = load_graph(&lines);
            value = part1(&graph);
        }
        assert_eq!(value, 19);
    }

    #[test]
    fn sample3_part1() {
        let mut value = 0usize;
        if let Ok(lines) = read_lines("./sample3.txt") {
            let graph = load_graph(&lines);
            value = part1(&graph);
        }
        assert_eq!(value, 226);
    }

    #[test]
    fn sample1_part2() {
        let mut value = 0usize;
        if let Ok(lines) = read_lines("./sample1.txt") {
            let graph = load_graph(&lines);
            value = part2(&graph);
        }
        assert_eq!(value, 36);
    }

    #[test]
    fn sample2_part2() {
        let mut value = 0usize;
        if let Ok(lines) = read_lines("./sample2.txt") {
            let graph = load_graph(&lines);
            value = part2(&graph);
        }
        assert_eq!(value, 103);
    }

    #[test]
    fn sample3_part2() {
        let mut value = 0usize;
        if let Ok(lines) = read_lines("./sample3.txt") {
            let graph = load_graph(&lines);
            value = part2(&graph);
        }
        assert_eq!(value, 3509);
    }
}
