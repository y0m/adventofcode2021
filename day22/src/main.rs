use crate::CubeState::*;
use std::{
    env::args,
    fs::File,
    io::{self, BufRead},
    ops::RangeInclusive,
    path::Path,
    time::Instant,
};

fn main() {
    let filename = args().nth(1).expect("program [filename]");
    if let Ok(lines) = read_lines(filename) {
        let steps = load_cuboids(&lines);

        let now = Instant::now();
        let on = part1(&steps);
        let elapsed = now.elapsed();
        println!("{} ns", elapsed.as_nanos());
        println!("{}", on);

        let now = Instant::now();
        let on = part2(&steps);
        let elapsed = now.elapsed();
        println!("{} ns", elapsed.as_nanos());
        println!("{}", on);
    }
}

fn part1(steps: &[RebootStep]) -> usize {
    let cr = Cuboid {
        x: -50..=50,
        y: -50..=50,
        z: -50..=50,
    };

    split_cuboids(steps)
        .into_iter()
        .map(|c| {
            Cuboid::new(
                &[c.xmin().max(cr.xmin()), c.xmax().min(cr.xmax())],
                &[c.ymin().max(cr.ymin()), c.ymax().min(cr.ymax())],
                &[c.zmin().max(cr.zmin()), c.zmax().min(cr.zmax())],
            )
        })
        .map(|c| c.volume())
        .sum()
}

fn part2(steps: &[RebootStep]) -> usize {
    split_cuboids(steps).into_iter().map(|c| c.volume()).sum()
}

#[derive(Debug)]
enum CubeState {
    On,
    Off,
}

impl From<&str> for CubeState {
    fn from(state: &str) -> Self {
        match state {
            "on" => On,
            "off" => Off,
            _ => unreachable!(),
        }
    }
}

#[derive(Debug, Clone)]
struct Cuboid {
    x: RangeInclusive<i64>,
    y: RangeInclusive<i64>,
    z: RangeInclusive<i64>,
}

impl Cuboid {
    fn new(xr: &[i64], yr: &[i64], zr: &[i64]) -> Self {
        Cuboid {
            x: xr[0]..=xr[1],
            y: yr[0]..=yr[1],
            z: zr[0]..=zr[1],
        }
    }

    fn subtract(&self, other: &Cuboid) -> Vec<Cuboid> {
        if !self.overlap(other) {
            vec![self.to_owned()]
        } else {
            [
                Cuboid::new(
                    &[self.xmin(), other.xmin() - 1],
                    &[self.ymin(), self.ymax()],
                    &[self.zmin(), self.zmax()],
                ),
                Cuboid::new(
                    &[other.x.end() + 1, self.xmax()],
                    &[self.ymin(), self.ymax()],
                    &[self.zmin(), self.zmax()],
                ),
                Cuboid::new(
                    &[self.xmin().max(other.xmin()), self.xmax().min(other.xmax())],
                    &[self.ymin(), other.ymin() - 1],
                    &[self.zmin(), self.zmax()],
                ),
                Cuboid::new(
                    &[self.xmin().max(other.xmin()), self.xmax().min(other.xmax())],
                    &[other.ymax() + 1, self.ymax()],
                    &[self.zmin(), self.zmax()],
                ),
                Cuboid::new(
                    &[self.xmin().max(other.xmin()), self.xmax().min(other.xmax())],
                    &[self.ymin().max(other.ymin()), self.ymax().min(other.ymax())],
                    &[self.zmin(), other.zmin() - 1],
                ),
                Cuboid::new(
                    &[self.xmin().max(other.xmin()), self.xmax().min(other.xmax())],
                    &[self.ymin().max(other.ymin()), self.ymax().min(other.ymax())],
                    &[other.zmax() + 1, self.zmax()],
                ),
            ]
            .into_iter()
            .filter(|c| !c.is_empty())
            .collect()
        }
    }

    fn is_empty(&self) -> bool {
        self.x.is_empty() || self.y.is_empty() || self.z.is_empty()
    }

    fn overlap(&self, other: &Cuboid) -> bool {
        !(other.x.start() > self.x.end()
            || other.x.end() < self.x.start()
            || other.y.start() > self.y.end()
            || other.y.end() < self.y.start()
            || other.z.start() > self.z.end()
            || other.z.end() < self.z.start())
    }

    fn volume(&self) -> usize {
        self.x.to_owned().count() * self.y.to_owned().count() * self.z.to_owned().count()
    }

    fn xmin(&self) -> i64 {
        self.x.start().to_owned()
    }

    fn xmax(&self) -> i64 {
        self.x.end().to_owned()
    }

    fn ymin(&self) -> i64 {
        self.y.start().to_owned()
    }

    fn ymax(&self) -> i64 {
        self.y.end().to_owned()
    }

    fn zmin(&self) -> i64 {
        self.z.start().to_owned()
    }

    fn zmax(&self) -> i64 {
        self.z.end().to_owned()
    }
}

#[derive(Debug)]
struct RebootStep {
    cuboid: Cuboid,
    state: CubeState,
}

impl From<&str> for RebootStep {
    fn from(step: &str) -> Self {
        let mut s = step.split_whitespace();

        Self {
            state: s.next().unwrap().into(),
            cuboid: s.next().unwrap().into(),
        }
    }
}

fn load_cuboids(lines: &[String]) -> Vec<RebootStep> {
    lines.iter().map(|c| c.as_str().into()).collect()
}

impl From<&str> for Cuboid {
    fn from(s: &str) -> Self {
        let sranges = s
            .to_owned()
            .replace("x=", "")
            .replace("y=", "")
            .replace("z=", "");
        let sranges = sranges
            .split(',')
            .map(|v| {
                v.split("..")
                    .map(|i| i.parse::<i64>().unwrap())
                    .collect::<Vec<_>>()
            })
            .collect::<Vec<_>>();
        Cuboid::new(&sranges[0], &sranges[1], &sranges[2])
    }
}

fn split_cuboids(steps: &[RebootStep]) -> Vec<Cuboid> {
    let mut splits = Vec::new();

    for s in steps {
        let newc = s.cuboid.to_owned();

        match s.state {
            On => {
                let mut parts = vec![newc];
                for split in splits.iter() {
                    let mut new_parts = Vec::new();
                    for p in parts {
                        new_parts.extend(p.subtract(split));
                    }
                    parts = new_parts;
                }
                splits.extend(parts);
            }
            Off => {
                let mut parts = Vec::new();
                for split in splits.into_iter() {
                    parts.extend(split.subtract(&newc));
                }
                splits = parts;
            }
        }
    }
    splits
}

fn read_lines<P>(filename: P) -> io::Result<Vec<String>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sample1_test() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample1.txt") {
            let cuboids = load_cuboids(&lines);
            value = part1(&cuboids);
        }
        assert_eq!(value, 39);
    }

    #[test]
    fn sample2_test() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample2.txt") {
            let cuboids = load_cuboids(&lines);
            value = part1(&cuboids);
        }
        assert_eq!(value, 590784);
    }

    #[test]
    fn sample3_test() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample3.txt") {
            let cuboids = load_cuboids(&lines);
            value = part2(&cuboids);
        }
        assert_eq!(value, 2758514936282235);
    }
}
