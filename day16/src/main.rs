#![allow(dead_code)]
use std::collections::VecDeque;
use std::env::args;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;
use std::str;
use std::time::Instant;

fn main() {
    let filename = args().nth(1).expect("program [filename]");
    if let Ok(lines) = read_lines(filename) {
        let now = Instant::now();
        let sum = part1(&lines[0]);
        let elapsed = now.elapsed();
        eprintln!("{} ns", elapsed.as_nanos());
        println!("{}", sum);

        let now = Instant::now();
        let sum = part2(&lines[0]);
        let elapsed = now.elapsed();
        eprintln!("{} ns", elapsed.as_nanos());
        println!("{}", sum);
    }
}

fn part1(hexa: &str) -> u64 {
    let mut sum = 0;
    let mut bits = init_bits(hexa.as_bytes());
    while !bits.is_empty() && !bits.iter().all(|&b| b == b'0') {
        let packet = get_packet(&mut bits);
        sum += sum_version(&packet);
    }
    sum
}

fn part2(hexa: &str) -> u64 {
    let mut sum = 0;
    let mut bits = init_bits(hexa.as_bytes());
    while !bits.is_empty() && !bits.iter().all(|&b| b == b'0') {
        let packet = get_packet(&mut bits);
        sum += type_rules(&packet);
    }
    sum
}

fn sum_version(packet: &Packet) -> u64 {
    match &packet.data {
        PacketData::Literal(_) => packet.v,
        PacketData::Operator(d) => packet.v + d.iter().map(sum_version).sum::<u64>(),
    }
}

fn type_rules(packet: &Packet) -> u64 {
    match &packet.data {
        PacketData::Literal(d) => *d,
        PacketData::Operator(d) => match packet.t {
            0 => d.iter().map(type_rules).sum::<u64>(),
            1 => d.iter().map(type_rules).product::<u64>(),
            2 => d.iter().map(type_rules).min().unwrap(),
            3 => d.iter().map(type_rules).max().unwrap(),
            5 => {
                let first = type_rules(&d[0]);
                let second = type_rules(&d[1]);
                (first > second) as u64
            }
            6 => {
                let first = type_rules(&d[0]);
                let second = type_rules(&d[1]);
                (first < second) as u64
            }
            7 => {
                let first = type_rules(&d[0]);
                let second = type_rules(&d[1]);
                (first == second) as u64
            }
            _ => panic!("invalid operator packet"),
        },
    }
}

fn get_packet(bits: &mut VecDeque<u8>) -> Packet {
    let v = u64::from_str_radix(
        str::from_utf8(&*bits.drain(..3).collect::<Vec<_>>()).unwrap(),
        2,
    )
    .unwrap();
    let t = u64::from_str_radix(
        str::from_utf8(&*bits.drain(..3).collect::<Vec<_>>()).unwrap(),
        2,
    )
    .unwrap();
    match t {
        4 => Packet {
            v,
            t,
            data: get_literal_data(bits),
        },
        _ => Packet {
            v,
            t,
            data: get_operator_data(bits),
        },
    }
}

fn get_operator_data(bits: &mut VecDeque<u8>) -> PacketData {
    let indicator = bits.pop_front().unwrap();
    let data = match indicator {
        b'1' => get_operator_ind1(bits),
        b'0' => get_operator_ind0(bits),
        _ => panic!("unknown indicator {:?}", indicator),
    };
    PacketData::Operator(data)
}

fn get_operator_ind0(bits: &mut VecDeque<u8>) -> Vec<Packet> {
    let len = u64::from_str_radix(
        str::from_utf8(&*bits.drain(..15).collect::<Vec<_>>()).unwrap(),
        2,
    )
    .unwrap();
    let slen = bits.len();
    let mut packets: Vec<Packet> = Vec::new();
    while (slen - bits.len()) as u64 != len {
        packets.push(get_packet(bits));
    }
    packets
}

fn get_operator_ind1(bits: &mut VecDeque<u8>) -> Vec<Packet> {
    let len = u64::from_str_radix(
        str::from_utf8(&*bits.drain(..11).collect::<Vec<_>>()).unwrap(),
        2,
    )
    .unwrap();

    let mut packets: Vec<Packet> = Vec::new();
    while packets.len() as u64 != len {
        packets.push(get_packet(bits));
    }
    packets
}

fn get_literal_data(bits: &mut VecDeque<u8>) -> PacketData {
    let mut data = Vec::new();
    while bits.len() >= 4 {
        let start = bits.pop_front().unwrap();
        data.append(&mut bits.drain(..4).collect());
        if start == b'0' {
            break;
        }
    }
    PacketData::Literal(u64::from_str_radix(str::from_utf8(&*data).unwrap(), 2).unwrap())
}

#[derive(Debug, Clone)]
struct Packet {
    v: u64,
    t: u64,
    data: PacketData,
}

#[derive(Debug, Clone)]
enum PacketData {
    // Literal Value
    Literal(u64),
    // Operator Packet
    Operator(Vec<Packet>),
}

fn init_bits(bytes: &[u8]) -> VecDeque<u8> {
    let mut bits: VecDeque<u8> = VecDeque::new();
    for &c in bytes {
        get_bits(c).into_iter().for_each(|b| bits.push_back(b));
    }
    bits
}

fn get_bits(c: u8) -> [u8; 4] {
    match c {
        b'0' => [b'0', b'0', b'0', b'0'],
        b'1' => [b'0', b'0', b'0', b'1'],
        b'2' => [b'0', b'0', b'1', b'0'],
        b'3' => [b'0', b'0', b'1', b'1'],
        b'4' => [b'0', b'1', b'0', b'0'],
        b'5' => [b'0', b'1', b'0', b'1'],
        b'6' => [b'0', b'1', b'1', b'0'],
        b'7' => [b'0', b'1', b'1', b'1'],
        b'8' => [b'1', b'0', b'0', b'0'],
        b'9' => [b'1', b'0', b'0', b'1'],
        b'A' => [b'1', b'0', b'1', b'0'],
        b'B' => [b'1', b'0', b'1', b'1'],
        b'C' => [b'1', b'1', b'0', b'0'],
        b'D' => [b'1', b'1', b'0', b'1'],
        b'E' => [b'1', b'1', b'1', b'0'],
        b'F' => [b'1', b'1', b'1', b'1'],
        _ => panic!("unknown hexa"),
    }
}

fn read_lines<P>(filename: P) -> io::Result<Vec<String>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sample1_part1() {
        assert_eq!(part1("D2FE28"), 6);
    }

    #[test]
    fn sample2_part1() {
        assert_eq!(part1("38006F45291200"), 9);
    }

    #[test]
    fn sample3_part1() {
        assert_eq!(part1("EE00D40C823060"), 14);
    }

    #[test]
    fn sample4_part1() {
        assert_eq!(part1("8A004A801A8002F478"), 16);
    }

    #[test]
    fn sample5_part1() {
        assert_eq!(part1("620080001611562C8802118E34"), 12);
    }

    #[test]
    fn sample6_part1() {
        assert_eq!(part1("C0015000016115A2E0802F182340"), 23);
    }

    #[test]
    fn sample7_part1() {
        assert_eq!(part1("A0016C880162017C3686B18A3D4780"), 31);
    }

    #[test]
    fn sample1_part2() {
        assert_eq!(part2("C200B40A82"), 3);
    }

    #[test]
    fn sample2_part2() {
        assert_eq!(part2("04005AC33890"), 54);
    }

    #[test]
    fn sample3_part2() {
        assert_eq!(part2("880086C3E88112"), 7);
    }

    #[test]
    fn sample4_part2() {
        assert_eq!(part2("CE00C43D881120"), 9);
    }

    #[test]
    fn sample5_part2() {
        assert_eq!(part2("D8005AC2A8F0"), 1);
    }

    #[test]
    fn sample6_part2() {
        assert_eq!(part2("F600BC2D8F"), 0);
    }

    #[test]
    fn sample7_part2() {
        assert_eq!(part2("9C005AC2F8F0"), 0);
    }

    #[test]
    fn sample8_part2() {
        assert_eq!(part2("9C0141080250320F1802104A08"), 1);
    }
}
