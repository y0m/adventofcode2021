use std::io::{self, Read};

mod solver;

fn main() {
    let input = get_input();

    solver::solve(&input, true); // part 1
    solver::solve(&input, false); // part 2
}

fn get_input() -> String {
    let mut input = String::new();
    io::stdin().lock().read_to_string(&mut input).unwrap();
    return input.trim().to_string();
}
