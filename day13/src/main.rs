#![allow(dead_code)]
use std::env::args;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;
use std::time::Instant;

fn main() {
    let filename = args().nth(1).expect("program [filename]");
    if let Ok(lines) = read_lines(filename) {
        let paper = load_paper(lines);
        let now = Instant::now();
        let dots = part1(&paper, Some(1));
        let elapsed = now.elapsed();
        eprintln!("{} ns", elapsed.as_nanos());
        println!("{}", dots.len());
        let now = Instant::now();
        part2(&paper);
        let elapsed = now.elapsed();
        eprintln!("{} ns", elapsed.as_nanos());
    }
}

fn part1(paper: &Paper, folds: Option<usize>) -> Vec<[usize; 2]> {
    // folds: None = all
    let mut dots = paper.dots.clone();
    let folds = if let Some(f) = folds {
        if f > paper.folds.len() {
            paper.folds.len()
        } else {
            f
        }
    } else {
        paper.folds.len()
    };
    for f in 0..folds {
        let fold = paper.folds[f];
        let (fi, fpos) = match fold {
            [Some(x), None] => (0usize, x),
            [None, Some(y)] => (1usize, y),
            _ => panic!("fold should be non exhaustive"),
        };
        let mut subdots = dots
            .iter()
            .enumerate()
            .filter(|(_, d)| d[fi] > fpos)
            .map(|(i, &d)| (i, d))
            .collect::<Vec<_>>()
            .clone();
        subdots.sort_by(|a, b| b.0.cmp(&a.0));
        for dot in subdots.iter_mut() {
            (*dot).1[fi] = fpos - ((*dot).1[fi] - fpos);
            if dots
                .iter()
                .filter(|&d| d[0] == dot.1[0] && d[1] == dot.1[1])
                .count()
                == 0
            {
                dots.push((*dot).1);
            }
            dots.remove(dot.0);
        }
    }
    dots
}

fn part2(paper: &Paper) {
    let dots = part1(paper, None);
    let xmax = dots.iter().map(|d| d[0]).max().unwrap() + 1;
    let ymax = dots.iter().map(|d| d[1]).max().unwrap() + 1;
    let mut grid = vec![vec![' '; xmax]; ymax];
    for dot in dots {
        grid[dot[1]][dot[0]] = '#';
    }
    println!();
    for y in grid {
        for x in y {
            print!("{}", x);
        }
        println!();
    }
}

#[derive(Debug)]
struct Paper {
    dots: Vec<[usize; 2]>,
    folds: Vec<[Option<usize>; 2]>,
}

fn load_paper(lines: Vec<String>) -> Paper {
    let mut p = Paper {
        dots: Vec::new(),
        folds: Vec::new(),
    };

    for line in lines {
        if line.is_empty() {
            continue;
        } else if !line.starts_with("fold along") {
            let mut dot = line.split(',').map(|a| a.parse::<usize>().unwrap());
            p.dots.push([dot.next().unwrap(), dot.next().unwrap()]);
        } else {
            let mut fold = line.split('=');
            if let Some(data) = fold.next() {
                let f = match data {
                    "fold along x" => [Some(fold.next().unwrap().parse::<usize>().unwrap()), None],
                    "fold along y" => [None, Some(fold.next().unwrap().parse::<usize>().unwrap())],
                    _ => panic!("unknown fold data"),
                };
                p.folds.push(f);
            }
        }
    }
    p
}

fn read_lines<P>(filename: P) -> io::Result<Vec<String>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sample_part1() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample.txt") {
            let paper = load_paper(lines);
            value = part1(&paper, Some(1)).len();
        }
        assert_eq!(value, 17);
    }
}
