use std::env::args;
use std::fs::File;
use std::io::{self, BufRead, Result};
use std::path::Path;
use std::time::Instant;

fn main() {
    let filename = args().nth(1).expect("program [filename]");
    if let Ok(lines) = read_lines(filename) {
        let now = Instant::now();
        let life = power_consumption(&lines);
        let elapsed = now.elapsed();
        eprintln!("{} ns", elapsed.as_nanos());
        println!("{}", life);
        let now = Instant::now();
        let life = life_support(&lines);
        let elapsed = now.elapsed();
        eprintln!("{} ns", elapsed.as_nanos());
        println!("{}", life);
    }
}

fn power_consumption(lines: &[String]) -> u64 {
    let len = lines.len();
    let mut gcounts = vec![0usize; lines[0].len()];
    lines.iter().map(|l| l.as_bytes()).for_each(|buf| {
        buf.iter()
            .enumerate()
            .filter(|(_, &b)| b == b'1')
            .for_each(|(i, _)| gcounts[i] += 1)
    });
    let mut gbits = vec![0u8; gcounts.len()];
    let mut ebits = vec![0u8; gcounts.len()];
    gcounts.iter().enumerate().for_each(|(pos, &count)| {
        if count > (len / 2) {
            gbits[pos] = b'1';
            ebits[pos] = b'0';
        } else {
            gbits[pos] = b'0';
            ebits[pos] = b'1';
        }
    });
    let gamma = u64::from_str_radix(std::str::from_utf8(&*gbits).unwrap(), 2).unwrap_or(0);
    let epsilon = u64::from_str_radix(std::str::from_utf8(&*ebits).unwrap(), 2).unwrap_or(0);

    gamma * epsilon
}

fn life_support(lines: &[String]) -> u64 {
    let glines = lines.iter().map(|x| x.as_bytes()).collect::<Vec<_>>();

    let ofn = |ones: usize, zeros: usize| {
        if ones >= zeros {
            b'1'
        } else {
            b'0'
        }
    };
    let ogrbits = criteria(&glines, 0, glines[0].len(), b'1', ofn);
    let cfn = |ones: usize, zeros: usize| {
        if zeros <= ones {
            b'0'
        } else {
            b'1'
        }
    };
    let csrbits = criteria(&glines, 0, glines[0].len(), b'0', cfn);

    let ogr = u64::from_str_radix(std::str::from_utf8(ogrbits).unwrap(), 2).unwrap_or(0);
    let csr = u64::from_str_radix(std::str::from_utf8(csrbits).unwrap(), 2).unwrap_or(0);

    ogr * csr
}

fn criteria<'a>(
    samples: &[&'a [u8]],
    pos: usize,
    len: usize,
    common: u8,
    critfn: impl Fn(usize, usize) -> u8,
) -> &'a [u8] {
    let ones = samples.iter().filter(|&&x| x[pos] == b'1').count();
    let zeros = samples.iter().filter(|&&x| x[pos] == b'0').count();
    let search = critfn(ones, zeros);
    let samples = samples
        .iter()
        .filter(|&&x| x[pos] == search)
        .copied()
        .collect::<Vec<_>>();
    if samples.len() == 1 {
        return samples[0];
    }
    let pos = (pos + 1) % len;
    criteria(&samples, pos, len, common, critfn)
}

fn read_lines<P>(filename: P) -> Result<Vec<String>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sample_test_part1() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample.txt") {
            value = power_consumption(&lines);
        }
        assert_eq!(value, 198);
    }

    #[test]
    fn sample_test_part2() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample.txt") {
            value = life_support(&lines);
        }
        assert_eq!(value, 230);
    }
}
