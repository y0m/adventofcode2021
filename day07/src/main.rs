#![allow(dead_code)]
use std::env::args;
use std::fs::File;
use std::io::{self, BufRead, Result};
use std::path::Path;
use std::time::Instant;

fn main() {
    let filename = args().nth(1).expect("program [filename]");
    if let Ok(lines) = read_lines(filename) {
        let crabs = load_crabs(lines);
        let now = Instant::now();
        let (fuel, pos) = lowest_consumption(&crabs, fuel_consumption1);
        let elapsed = now.elapsed();
        eprintln!("{} ns", elapsed.as_nanos());
        println!("{} {}", pos, fuel);
        let now = Instant::now();
        let (fuel, pos) = lowest_consumption(&crabs, fuel_consumption2);
        let elapsed = now.elapsed();
        eprintln!("{} ns", elapsed.as_nanos());
        println!("{} {}", pos, fuel);
    }
}

fn read_lines<P>(filename: P) -> Result<Vec<String>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect())
}

fn load_crabs(lines: Vec<String>) -> Vec<usize> {
    lines[0]
        .split(',')
        .map(|x| x.parse::<usize>().unwrap())
        .collect()
}

fn fuel_consumption1(crabs: &[usize], position: usize) -> usize {
    let mut fuel = 0;
    for c in crabs {
        if c < &position {
            fuel += position - c;
        } else {
            fuel += c - position;
        }
    }
    fuel
}

fn fuel_consumption2(crabs: &[usize], position: usize) -> usize {
    let mut fuel = 0;
    for &c in crabs {
        let mut amount = 1;
        let (from, to) = if c < position {
            (c, position)
        } else {
            (position, c)
        };
        for _p in from..to {
            fuel += amount;
            amount += 1;
        }
    }
    fuel
}

fn lowest_consumption(
    crabs: &[usize],
    fuelfn: impl Fn(&[usize], usize) -> usize,
) -> (usize, usize) {
    let max = *crabs.iter().max().unwrap();
    let mut lowest = usize::MAX;
    let mut position = 0;
    for pos in 1..=max {
        let fuel = fuelfn(crabs, pos);
        if fuel <= lowest {
            lowest = fuel;
            position = pos;
        }
    }
    (lowest, position)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sample_test_1_2() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample.txt") {
            let crabs = load_crabs(lines);
            value = fuel_consumption1(&crabs, 2);
        }
        assert_eq!(value, 37);
    }
    #[test]
    fn sample_test_1_1() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample.txt") {
            let crabs = load_crabs(lines);
            value = fuel_consumption1(&crabs, 1);
        }
        assert_eq!(value, 41);
    }
    #[test]
    fn sample_test_1_3() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample.txt") {
            let crabs = load_crabs(lines);
            value = fuel_consumption1(&crabs, 3);
        }
        assert_eq!(value, 39);
    }
    #[test]
    fn sample_test_1_10() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample.txt") {
            let crabs = load_crabs(lines);
            value = fuel_consumption1(&crabs, 10);
        }
        assert_eq!(value, 71);
    }

    #[test]
    fn sample_test_2_2() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample.txt") {
            let crabs = load_crabs(lines);
            value = fuel_consumption2(&crabs, 2);
        }
        assert_eq!(value, 206);
    }
    #[test]
    fn sample_test_2_5() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample.txt") {
            let crabs = load_crabs(lines);
            value = fuel_consumption2(&crabs, 5);
        }
        assert_eq!(value, 168);
    }
}
