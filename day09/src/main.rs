#![allow(dead_code)]
use std::env::args;
use std::fs::File;
use std::io::{self, BufRead, Result};
use std::path::Path;
use std::time::Instant;

fn main() {
    let filename = args().nth(1).expect("program [filename]");
    if let Ok(lines) = read_lines(filename) {
        let (heightmap, bounds) = load_heightmap(lines);
        let now = Instant::now();
        let sum = risk_levels_sum(&heightmap, bounds);
        let elapsed = now.elapsed();
        eprintln!("{} ns", elapsed.as_nanos());
        println!("{}", sum);
        let now = Instant::now();
        let prod = three_largest_bassins(&heightmap, bounds);
        let elapsed = now.elapsed();
        eprintln!("{} ns", elapsed.as_nanos());
        println!("{}", prod);
    }
}

fn load_heightmap(lines: Vec<String>) -> (Vec<usize>, (usize, usize)) {
    let mut heightmap = Vec::new();
    for line in &lines {
        line.as_bytes()
            .iter()
            .for_each(|&b| heightmap.push((b - b'0') as usize));
    }
    (heightmap, (lines[0].len(), lines.len()))
}

fn three_largest_bassins(heightmap: &[usize], bounds: (usize, usize)) -> usize {
    let mut bassins = Vec::new();
    let mut lowest = Vec::new();
    for (index, &height) in heightmap.iter().enumerate() {
        let adj = adjacent(heightmap, bounds, index);
        let count = adj
            .iter()
            .filter(|&&a| a.is_some() && a.unwrap() <= height)
            .count();
        if count == 0 {
            lowest.push(index);
        }
    }
    for index in lowest {
        let mut bassin = Vec::new();
        explore(heightmap, bounds, index, &mut bassin);
        bassins.push(bassin.len());
    }
    bassins.sort_unstable();
    let start = bassins.len() - 3;
    bassins[start..].iter().product()
}

fn explore(heightmap: &[usize], bounds: (usize, usize), pos: usize, bassin: &mut Vec<usize>) {
    if bassin.iter().any(|&l| l == pos) {
        return;
    }
    bassin.push(pos);
    let pos = ((pos % bounds.0) as i64, (pos / bounds.0) as i64);
    if let Some(up) = get_up_tuple(heightmap, bounds, pos) {
        if up.1 != 9 {
            explore(heightmap, bounds, up.0, bassin);
        }
    }
    if let Some(down) = get_down_tuple(heightmap, bounds, pos) {
        if down.1 != 9 {
            explore(heightmap, bounds, down.0, bassin);
        }
    }
    if let Some(left) = get_left_tuple(heightmap, bounds, pos) {
        if left.1 != 9 {
            explore(heightmap, bounds, left.0, bassin);
        }
    }
    if let Some(right) = get_right_tuple(heightmap, bounds, pos) {
        if right.1 != 9 {
            explore(heightmap, bounds, right.0, bassin);
        }
    }
}

fn risk_levels_sum(heightmap: &[usize], bounds: (usize, usize)) -> usize {
    let mut risks = Vec::new();
    for (index, &height) in heightmap.iter().enumerate() {
        let adj = adjacent(heightmap, bounds, index);
        let count = adj
            .iter()
            .filter(|&&a| a.is_some() && a.unwrap() <= height)
            .count();
        if count == 0 {
            risks.push(height + 1);
        }
    }
    risks.iter().sum()
}

fn adjacent(heightmap: &[usize], bounds: (usize, usize), pos: usize) -> Vec<Option<usize>> {
    let pos = ((pos % bounds.0) as i64, (pos / bounds.0) as i64);
    vec![
        get_up(heightmap, bounds, pos),
        get_down(heightmap, bounds, pos),
        get_left(heightmap, bounds, pos),
        get_right(heightmap, bounds, pos),
    ]
}

fn get_up_tuple(
    heightmap: &[usize],
    bounds: (usize, usize),
    pos: (i64, i64),
) -> Option<(usize, usize)> {
    let up = (pos.0, pos.1 - 1);
    if up.1 < 0 {
        None
    } else {
        let index = (up.0 as usize + up.1 as usize * bounds.0) as usize;
        Some((index, heightmap[index]))
    }
}

fn get_down_tuple(
    heightmap: &[usize],
    bounds: (usize, usize),
    pos: (i64, i64),
) -> Option<(usize, usize)> {
    let down = (pos.0, pos.1 + 1);
    if down.1 as usize >= bounds.1 {
        None
    } else {
        let index = (down.0 as usize + down.1 as usize * bounds.0) as usize;
        Some((index, heightmap[index]))
    }
}

fn get_left_tuple(
    heightmap: &[usize],
    bounds: (usize, usize),
    pos: (i64, i64),
) -> Option<(usize, usize)> {
    let left = (pos.0 - 1, pos.1);
    if left.0 < 0 {
        None
    } else {
        let index = (left.0 as usize + left.1 as usize * bounds.0) as usize;
        Some((index, heightmap[index]))
    }
}

fn get_right_tuple(
    heightmap: &[usize],
    bounds: (usize, usize),
    pos: (i64, i64),
) -> Option<(usize, usize)> {
    let right = (pos.0 + 1, pos.1);
    if right.0 as usize >= bounds.0 {
        None
    } else {
        let index = (right.0 as usize + right.1 as usize * bounds.0) as usize;
        Some((index, heightmap[index]))
    }
}

fn get_up(heightmap: &[usize], bounds: (usize, usize), pos: (i64, i64)) -> Option<usize> {
    let up = (pos.0, pos.1 - 1);
    if up.1 < 0 {
        None
    } else {
        let index = (up.0 as usize + up.1 as usize * bounds.0) as usize;
        Some(heightmap[index])
    }
}

fn get_down(heightmap: &[usize], bounds: (usize, usize), pos: (i64, i64)) -> Option<usize> {
    let down = (pos.0, pos.1 + 1);
    if down.1 as usize >= bounds.1 {
        None
    } else {
        let index = (down.0 as usize + down.1 as usize * bounds.0) as usize;
        Some(heightmap[index])
    }
}

fn get_left(heightmap: &[usize], bounds: (usize, usize), pos: (i64, i64)) -> Option<usize> {
    let left = (pos.0 - 1, pos.1);
    if left.0 < 0 {
        None
    } else {
        let index = (left.0 as usize + left.1 as usize * bounds.0) as usize;
        Some(heightmap[index])
    }
}

fn get_right(heightmap: &[usize], bounds: (usize, usize), pos: (i64, i64)) -> Option<usize> {
    let right = (pos.0 + 1, pos.1);
    if right.0 as usize >= bounds.0 {
        None
    } else {
        let index = (right.0 as usize + right.1 as usize * bounds.0) as usize;
        Some(heightmap[index])
    }
}

fn read_lines<P>(filename: P) -> Result<Vec<String>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sample_part1() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample.txt") {
            let (heightmap, bounds) = load_heightmap(lines);
            value = risk_levels_sum(&heightmap, bounds);
        }
        assert_eq!(value, 15);
    }

    #[test]
    fn sample_part2() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample.txt") {
            let (heightmap, bounds) = load_heightmap(lines);
            value = three_largest_bassins(&heightmap, bounds);
        }
        assert_eq!(value, 1134);
    }
}
