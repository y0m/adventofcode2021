#![allow(dead_code)]
use std::env::args;
use std::fs::File;
use std::io::{self, BufRead, Result};
use std::path::Path;
use std::time::Instant;

fn main() {
    let filename = args().nth(1).expect("program [filename]");
    if let Ok(lines) = read_lines(filename) {
        let records = load_data(&lines);
        let now = Instant::now();
        let count = part1(&records);
        let elapsed = now.elapsed();
        eprintln!("{} ns", elapsed.as_nanos());
        println!("{}", count);
        let now = Instant::now();
        let count = part2(&records);
        let elapsed = now.elapsed();
        eprintln!("{} ns", elapsed.as_nanos());
        println!("{}", count);
    }
}

#[derive(Clone, Copy)]
struct Digit<'a> {
    segments: &'a str,
}

const DIGITS: &[Digit; 10] = &[
    Digit { segments: "abcefg" },
    Digit { segments: "cf" },
    Digit { segments: "acdeg" },
    Digit { segments: "acdfg" },
    Digit { segments: "bcdf" },
    Digit { segments: "abdfg" },
    Digit { segments: "abdefg" },
    Digit { segments: "acf" },
    Digit {
        segments: "abcdefg",
    },
    Digit { segments: "abcdfg" },
];

fn possible_digits(signal: &str) -> Vec<usize> {
    match signal.len() {
        2 => vec![1],
        3 => vec![7],
        4 => vec![4],
        5 => vec![2, 3, 5],
        6 => vec![0, 6, 9],
        7 => vec![8],
        _ => panic!("invalid signal"),
    }
}

fn signal_to_num(signal: &str) -> usize {
    let mut s: Vec<u8> = signal.bytes().collect();
    s.sort_unstable();
    match DIGITS
        .iter()
        .enumerate()
        .find(|(_, &d)| d.segments == String::from_utf8_lossy(&s))
    {
        Some(digit) => digit.0,
        None => panic!("invalid signal"),
    }
}

#[derive(Debug)]
struct Data<'a> {
    signals: Vec<&'a str>,
    digits: Vec<&'a str>,
    mapping: Vec<Vec<char>>,
}

fn segments_init() -> Vec<Vec<char>> {
    (0..7).map(|_| "abcdefg".chars().collect()).collect()
}

fn load_data(lines: &[String]) -> Vec<Data> {
    let mut list = Vec::new();
    lines.iter().for_each(|x| {
        let values = x.split(" | ").collect::<Vec<_>>();
        let mut data = Data {
            signals: values[0].split_whitespace().collect(),
            digits: values[1].split_whitespace().collect(),
            mapping: vec![],
        };
        data.signals.sort_by(|&a, &b| a.len().cmp(&b.len()));
        let mut mapping: Vec<Vec<char>> =
            data.signals.iter().fold(segments_init(), |mapp, &signal| {
                possible_digits(signal).iter().fold(mapp, |m, &d| {
                    let dseg: Vec<u8> = DIGITS[d].segments.bytes().map(|b| b - b'a').collect();
                    let segments = m
                        .iter()
                        .enumerate()
                        .map(|(i, s)| {
                            s.iter()
                                .filter(|&c| {
                                    if dseg.contains(&(i as u8)) {
                                        signal.contains(&c.to_string())
                                    } else {
                                        !signal.contains(&c.to_string())
                                    }
                                })
                                .copied()
                                .collect::<Vec<_>>()
                        })
                        .collect::<Vec<_>>();
                    if segments.iter().all(|s| !s.is_empty()) {
                        return segments;
                    }
                    m
                })
            });
        data.mapping.append(&mut mapping);
        list.push(data);
    });
    list
}

fn mapping_signal(mapping: &[Vec<char>], signal: &str) -> usize {
    signal_to_num(
        &signal
            .chars()
            .map(|c| (mapping.iter().flatten().position(|&s| s == c).unwrap() as u8 + b'a') as char)
            .collect::<String>(),
    )
}

fn part1(data: &[Data]) -> usize {
    let seglen = &[1, 4, 7, 8].map(|d| DIGITS[d].segments.len());
    data.iter()
        .map(|d| {
            d.digits
                .iter()
                .filter(|&&v| seglen.contains(&v.len()))
                .count()
        })
        .sum()
}

fn part2(data: &[Data]) -> usize {
    data.iter()
        .map(|dt| {
            dt.digits
                .iter()
                .map(|d| mapping_signal(&dt.mapping, d).to_string())
                .collect::<String>()
        })
        .map(|n| n.parse::<usize>().unwrap())
        .sum::<usize>()
}

fn read_lines<P>(filename: P) -> Result<Vec<String>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sample_part1() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample.txt") {
            let data = load_data(&lines);
            value = part1(&data);
        }
        assert_eq!(value, 26);
    }

    #[test]
    fn sample_part2() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample.txt") {
            let data = load_data(&lines);
            value = part2(&data);
        }
        assert_eq!(value, 61229);
    }
}
