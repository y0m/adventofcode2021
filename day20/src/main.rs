use std::{
    collections::HashSet,
    env::args,
    error::Error,
    fs::File,
    io::{self, Read},
    time::Instant,
};

fn main() -> Result<(), Box<dyn Error>> {
    let filename = args().nth(1);
    if let Some(f) = filename {
        let content = read_file(&f)?;
        let iea = Enchancement::new(&content);

        let now = Instant::now();
        println!("{}", part1(&iea));
        println!("{} ns", now.elapsed().as_nanos());

        let now = Instant::now();
        println!("{}", part2(&iea));
        println!("{} ns", now.elapsed().as_nanos());

        Ok(())
    } else {
        Err("usage: program [filename]".into())
    }
}

fn part1(iea: &Enchancement) -> usize {
    let mut iea = iea.clone();
    iea.enchance(2)
}

fn part2(iea: &Enchancement) -> usize {
    let mut iea = iea.clone();
    iea.enchance(50)
}

#[derive(Debug, Clone)]
struct Enchancement {
    algo: Vec<char>,
    image: HashSet<(i64, i64)>,
    rowmin: i64,
    rowmax: i64,
    colmin: i64,
    colmax: i64,
}

impl Enchancement {
    fn new(input: &str) -> Self {
        let mut lines = input.lines();
        let algo = lines.next().unwrap().chars().collect();
        lines.next();

        let mut image = HashSet::new();
        let mut rowmin = i64::MAX;
        let mut rowmax = i64::MIN;
        let mut colmin = i64::MAX;
        let mut colmax = i64::MIN;
        for (row, line) in lines.enumerate() {
            for (col, char) in line.chars().enumerate() {
                if char == '#' {
                    rowmin = rowmin.min(row as i64);
                    rowmax = rowmax.max(row as i64);
                    colmin = colmin.min(col as i64);
                    colmax = colmax.max(col as i64);
                    image.insert((row as i64, col as i64));
                }
            }
        }

        Self {
            algo,
            image,
            rowmin,
            rowmax,
            colmin,
            colmax,
        }
    }

    fn enchance(&mut self, times: usize) -> usize {
        for step in 0..times {
            let (track, exists_val, not_exists_val) = if self.algo[0] == '#' {
                if step % 2 == 0 {
                    ('.', '1', '0')
                } else {
                    ('#', '0', '1')
                }
            } else {
                ('#', '1', '0')
            };

            let mut next: HashSet<(i64, i64)> = HashSet::new();
            let mut nrowmin = i64::MAX;
            let mut nrowmax = i64::MIN;
            let mut ncolmin = i64::MAX;
            let mut ncolmax = i64::MIN;

            for out_row in (self.rowmin - 1)..=(self.rowmax + 1) {
                for out_col in (self.colmin - 1)..=(self.colmax + 1) {
                    let mut digits: Vec<char> = Vec::new();
                    for i in (out_row - 1)..=(out_row + 1) {
                        for j in (out_col - 1)..=(out_col + 1) {
                            if self.image.contains(&(i, j)) {
                                digits.push(exists_val);
                            } else {
                                digits.push(not_exists_val);
                            }
                        }
                    }

                    let binary: String = digits.iter().collect();
                    let index = usize::from_str_radix(&binary, 2).unwrap();

                    if self.algo[index] == track {
                        next.insert((out_row, out_col));

                        nrowmin = nrowmin.min(out_row as i64);
                        nrowmax = nrowmax.max(out_row as i64);
                        ncolmin = ncolmin.min(out_col as i64);
                        ncolmax = ncolmax.max(out_col as i64);
                    }
                }
            }

            self.image = next;
            self.rowmin = nrowmin;
            self.rowmax = nrowmax;
            self.colmin = ncolmin;
            self.colmax = ncolmax;
        }

        self.image.len()
    }
}

fn read_file(filename: &str) -> io::Result<String> {
    let mut buf = String::new();
    File::open(filename)?.read_to_string(&mut buf)?;
    Ok(buf)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sample_part1() {
        let mut value = 0;
        if let Ok(content) = read_file("./sample.txt") {
            let iea = Enchancement::new(&content);
            value = part1(&iea);
        }
        assert_eq!(value, 35);
    }

    #[test]
    fn sample_part2() {
        let mut value = 0;
        if let Ok(content) = read_file("./sample.txt") {
            let iea = Enchancement::new(&content);
            value = part2(&iea);
        }
        assert_eq!(value, 3351);
    }
}
