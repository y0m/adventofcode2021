use std::{
    env::args,
    error::Error,
    fs::File,
    io::{self, Read},
    time::Instant,
};

fn main() -> Result<(), Box<dyn Error>> {
    let filename = args().nth(1).expect("usage: program [filename]");
    let content = read_file(&filename)?;
    let cucumbers = load_cucumbers(&content);
    let now = Instant::now();
    println!("{}", part1(&cucumbers));
    println!("{} ns", now.elapsed().as_nanos());
    Ok(())
}

fn part1(cucumbers: &[Vec<char>]) -> usize {
    let mut cucs = cucumbers.to_owned();
    let mut step = 0;
    loop {
        let (east, east_moved) = east_move(&cucs);
        let (south, south_moved) = south_move(&east);
        step += 1;
        if !east_moved && !south_moved {
            break;
        }
        cucs = south;
    }
    step
}

fn east_move(cucumbers: &[Vec<char>]) -> (Vec<Vec<char>>, bool) {
    let mut moved = false;
    let mut next = cucumbers.to_owned();

    for (i, l) in cucumbers.iter().enumerate() {
        for (j, &c) in l.iter().enumerate() {
            if c != '>' {
                continue;
            }

            let nextcol = (j + 1) % l.len();
            if l[nextcol] != '.' {
                continue;
            }

            next[i][j] = '.';
            next[i][nextcol] = '>';
            moved = true;
        }
    }
    (next, moved)
}

fn south_move(cucumbers: &[Vec<char>]) -> (Vec<Vec<char>>, bool) {
    let len = cucumbers.len();
    let mut moved = false;
    let mut next = cucumbers.to_owned();

    for (i, l) in cucumbers.iter().enumerate() {
        for (j, &c) in l.iter().enumerate() {
            if c != 'v' {
                continue;
            }

            let nextrow = (i + 1) % len;
            if cucumbers[nextrow][j] != '.' {
                continue;
            }

            next[i][j] = '.';
            next[nextrow][j] = 'v';
            moved = true;
        }
    }
    (next, moved)
}

fn load_cucumbers(content: &str) -> Vec<Vec<char>> {
    content.lines().map(|l| l.chars().collect()).collect()
}

fn read_file(filename: &str) -> io::Result<String> {
    let mut content = String::new();
    File::open(filename)?.read_to_string(&mut content)?;
    Ok(content)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sample_part1() {
        let mut value = 0;
        if let Ok(content) = read_file("./sample.txt") {
            let cuc = load_cucumbers(&content);
            value = part1(&cuc);
        }
        assert_eq!(value, 58);
    }
}
