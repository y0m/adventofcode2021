#![allow(dead_code)]
#![feature(drain_filter)]
use std::env::args;
use std::fs::File;
use std::io::{self, BufRead, Result};
use std::path::Path;
use std::time::Instant;

fn main() {
    let filename = args().nth(1).expect("program [filename]");
    if let Ok(lines) = read_lines(filename) {
        let now = Instant::now();
        let sum = part1(&lines);
        let elapsed = now.elapsed();
        eprintln!("{} ns", elapsed.as_nanos());
        println!("{}", sum);
        let now = Instant::now();
        let score = part2(lines);
        let elapsed = now.elapsed();
        eprintln!("{} ns", elapsed.as_nanos());
        println!("{}", score);
    }
}

#[derive(PartialEq)]
enum Bracket {
    Round,
    Square,
    Curl,
    Angle,
}

fn part1(lines: &[Vec<u8>]) -> usize {
    let mut score = 0;
    for line in lines {
        let mut checker: Vec<Bracket> = Vec::new();
        for b in line {
            let open = match *b {
                b'[' | b'(' | b'{' | b'<' => true,
                b']' | b')' | b'}' | b'>' => false,
                _ => panic!("invalid bracket type"),
            };
            let bt = match *b {
                b'(' | b')' => Bracket::Round,
                b'[' | b']' => Bracket::Square,
                b'{' | b'}' => Bracket::Curl,
                b'<' | b'>' => Bracket::Angle,
                _ => panic!("invalid bracket type"),
            };
            if open {
                checker.push(bt);
            } else {
                let closer = checker.pop().unwrap();
                if closer != bt {
                    score += match bt {
                        Bracket::Round => 3,
                        Bracket::Square => 57,
                        Bracket::Curl => 1197,
                        Bracket::Angle => 25137,
                    };
                    break;
                }
            }
        }
    }
    score
}

fn part2(mut lines: Vec<Vec<u8>>) -> usize {
    let _corrupted = lines
        .drain_filter(|line| {
            let mut checker: Vec<Bracket> = Vec::new();
            for b in line {
                let open = match *b {
                    b'[' | b'(' | b'{' | b'<' => true,
                    b']' | b')' | b'}' | b'>' => false,
                    _ => panic!("invalid bracket type"),
                };
                let bt = match *b {
                    b'(' | b')' => Bracket::Round,
                    b'[' | b']' => Bracket::Square,
                    b'{' | b'}' => Bracket::Curl,
                    b'<' | b'>' => Bracket::Angle,
                    _ => panic!("invalid bracket type"),
                };
                if open {
                    checker.push(bt);
                } else {
                    let closer = checker.pop().unwrap();
                    if closer != bt {
                        return true;
                    }
                }
            }
            false
        })
        .collect::<Vec<_>>();
    let mut scores = Vec::new();
    for line in lines {
        let mut score = 0;
        let mut checker: Vec<Bracket> = Vec::new();
        for b in line {
            let open = match b {
                b'[' | b'(' | b'{' | b'<' => true,
                b']' | b')' | b'}' | b'>' => false,
                _ => panic!("invalid bracket type"),
            };
            let bt = match b {
                b'(' | b')' => Bracket::Round,
                b'[' | b']' => Bracket::Square,
                b'{' | b'}' => Bracket::Curl,
                b'<' | b'>' => Bracket::Angle,
                _ => panic!("invalid bracket type"),
            };
            if open {
                checker.push(bt);
            } else {
                checker.pop();
            }
        }
        loop {
            let bt = checker.pop();
            if bt.is_none() {
                break;
            }
            score *= 5;
            score += match bt.unwrap() {
                Bracket::Round => 1,
                Bracket::Square => 2,
                Bracket::Curl => 3,
                Bracket::Angle => 4,
            };
        }
        scores.push(score);
    }
    let len = scores.len();
    scores.sort_unstable();
    if len > 0 {
        return scores[len / 2];
    }
    0
}

fn read_lines<P>(filename: P) -> Result<Vec<Vec<u8>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .map(|x| x.as_bytes().to_owned())
        .collect())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sample_part1() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample.txt") {
            value = part1(&lines);
        }
        assert_eq!(value, 26397);
    }

    #[test]
    fn sample_part2() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample.txt") {
            value = part2(lines);
        }
        assert_eq!(value, 288957);
    }
}
