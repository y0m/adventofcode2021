#![allow(dead_code)]
use std::env::args;
use std::fs::File;
use std::io::{self, BufRead, Result};
use std::path::Path;
use std::time::Instant;

fn main() {
    let filename = args().nth(1).expect("program [filename] [days]");
    let days = args()
        .nth(2)
        .expect("program [filename] [days]")
        .parse::<usize>()
        .unwrap();
    if let Ok(lines) = read_lines(filename) {
        let fishes = load_lanternfishes(lines);
        let now = Instant::now();
        let count = growth(fishes, days);
        let elapsed = now.elapsed();
        eprintln!("{} ns", elapsed.as_nanos());
        println!("{}", count);
    }
}

fn load_lanternfishes(lines: Vec<String>) -> Vec<u8> {
    lines[0]
        .split(',')
        .map(|x| x.parse::<u8>().unwrap())
        .collect()
}

fn growth(fishes: Vec<u8>, days: usize) -> usize {
    let mut counts = vec![0; 9];
    for f in fishes {
        counts[f as usize] += 1;
    }
    for _ in 1..=days {
        let c0 = counts[0];
        for i in 1..9 {
            counts[i - 1] = counts[i];
        }
        counts[8] = c0;
        counts[6] += counts[8];
    }
    counts.iter().sum()
}

fn read_lines<P>(filename: P) -> Result<Vec<String>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sample_test_18() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample.txt") {
            let fishes = load_lanternfishes(lines);
            value = growth(fishes, 18);
        }
        assert_eq!(value, 26);
    }
    #[test]
    fn sample_test_80() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample.txt") {
            let fishes = load_lanternfishes(lines);
            value = growth(fishes, 80);
        }
        assert_eq!(value, 5934);
    }
    #[test]
    fn sample_test_256() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample.txt") {
            let fishes = load_lanternfishes(lines);
            value = growth(fishes, 256);
        }
        assert_eq!(value, 26984457539);
    }
}
