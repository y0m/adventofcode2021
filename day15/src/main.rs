use std::env::args;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;
use std::time::Instant;

fn main() {
    let filename = args().nth(1).expect("program [filename]");
    if let Ok(lines) = read_lines(filename) {
        let graph = load_graph(&lines);
        let now = Instant::now();
        let risk = part1(&graph);
        let elapsed = now.elapsed();
        eprintln!("{} ns", elapsed.as_nanos());
        println!("{}", risk);
        let graph = load_5by5(graph);
        let now = Instant::now();
        let risk = part1(&graph);
        let elapsed = now.elapsed();
        eprintln!("{} ns", elapsed.as_nanos());
        println!("{}", risk);
    }
}

fn part1(graph: &[Vec<usize>]) -> usize {
    // Dijkstra: positive weights
    let xmax = graph[0].len();
    let ymax = graph.len();
    let start: (usize, usize) = (0, 0);
    let end: (usize, usize) = (xmax - 1, ymax - 1);

    // Init.
    let mut p: Vec<Vec<Option<(usize, usize)>>> = vec![vec![None; xmax]; ymax];
    let mut d = vec![vec![usize::MAX; xmax]; ymax];
    d[0][0] = 0;

    // Algo
    let mut q: Vec<(usize, usize)> = Vec::new();
    for (y, r) in graph.iter().enumerate() {
        for (x, _) in r.iter().enumerate() {
            q.push((x, y));
        }
    }
    let mut neighs: [Option<(usize, usize)>; 4] = [None; 4];
    while !q.is_empty() {
        let s1 = q.remove(find_min(&q, &d));
        neighbors(&s1, &(xmax, ymax), &mut neighs);
        for s2 in neighs.into_iter().flatten() {
            let dist = d[s1.0][s1.1] + (graph[s1.0][s1.1] + graph[s2.0][s2.1]);
            if d[s2.0][s2.1] > dist {
                d[s2.0][s2.1] = dist;
                p[s2.0][s2.1] = Some(s1);
            }
        }
    }

    // Shortest path
    let mut a: Vec<usize> = Vec::new();
    let mut s = end;
    while s != start {
        if let Some(prev) = p[s.0][s.1] {
            a.insert(0, graph[s.0][s.1]);
            s = prev;
            continue;
        }
        break;
    }

    a.iter().sum()
}

fn load_5by5(orig: Vec<Vec<usize>>) -> Vec<Vec<usize>> {
    let bounds = (orig[0].len(), orig.len());
    let mut graph = vec![vec![0; 5 * bounds.0]; 5 * bounds.1];
    for j in 0..5 {
        for i in 0..5 {
            for (y, r) in orig.iter().enumerate() {
                for (x, &v) in r.iter().enumerate() {
                    let x = x + i * bounds.0;
                    let y = y + j * bounds.1;
                    graph[y][x] = (((v - 1) + (i + j)) % 9) + 1;
                }
            }
        }
    }
    graph
}

fn neighbors(s: &(usize, usize), bounds: &(usize, usize), neighs: &mut [Option<(usize, usize)>]) {
    neighs.fill(None);
    if s.0 > 0 {
        // left
        neighs[0] = Some((s.0 - 1, s.1));
    }
    if s.0 < bounds.0 - 1 {
        // right
        neighs[1] = Some((s.0 + 1, s.1));
    }
    if s.1 > 0 {
        // up
        neighs[2] = Some((s.0, s.1 - 1));
    }
    if s.1 < bounds.1 - 1 {
        // down
        neighs[3] = Some((s.0, s.1 + 1));
    }
}

fn find_min(q: &[(usize, usize)], d: &[Vec<usize>]) -> usize {
    let mut min = usize::MAX;
    let mut idx = usize::MAX;
    for (i, s) in q.iter().enumerate() {
        if d[s.0][s.1] < min {
            min = d[s.0][s.1];
            idx = i;
        }
    }
    idx
}

fn load_graph(lines: &[String]) -> Vec<Vec<usize>> {
    lines
        .iter()
        .map(|l| l.bytes().map(|b| (b - b'0') as usize).collect::<Vec<_>>())
        .collect()
}

fn read_lines<P>(filename: P) -> io::Result<Vec<String>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sample_part1() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample.txt") {
            let graph = load_graph(&lines);
            value = part1(&graph);
        }
        assert_eq!(value, 40);
    }

    #[test]
    fn sample_part2() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample.txt") {
            let graph = load_5by5(load_graph(&lines));
            value = part1(&graph);
        }
        assert_eq!(value, 315);
    }
}
