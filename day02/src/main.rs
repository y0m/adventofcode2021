use std::env::args;
use std::fs::File;
use std::io::{self, BufRead, Result};
use std::path::Path;
use std::time::Instant;

fn main() {
    let filename = args().nth(1).expect("program [filename]");
    if let Ok(lines) = read_lines(filename) {
        let now = Instant::now();
        let product = pilot_part1(&lines);
        let elapsed = now.elapsed();
        eprintln!("{} ns", elapsed.as_nanos());
        println!("{}", product);
        let now = Instant::now();
        let product = pilot_part2(&lines);
        let elapsed = now.elapsed();
        eprintln!("{} ns", elapsed.as_nanos());
        println!("{}", product);
    }
}

fn pilot_part1(lines: &[String]) -> i64 {
    let mut position = (0, 0);
    for line in lines {
        let values = &*line.split_whitespace().collect::<Vec<_>>();
        let units = values[1].parse::<i64>().unwrap();
        match values[0] {
            "forward" => position.0 += units,
            "down" => position.1 += units,
            "up" => position.1 -= units,
            _ => continue,
        }
    }
    position.0 * position.1
}

fn pilot_part2(lines: &[String]) -> i64 {
    let mut position = (0, 0, 0);
    for line in lines {
        let values = &*line.split_whitespace().collect::<Vec<_>>();
        let units = values[1].parse::<i64>().unwrap();
        match values[0] {
            "forward" => {
                position.0 += units;
                position.1 += position.2 * units;
            }
            "down" => position.2 += units,
            "up" => position.2 -= units,
            _ => continue,
        }
    }
    position.0 * position.1
}

fn read_lines<P>(filename: P) -> Result<Vec<String>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sample_test_part1() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample.txt") {
            value = pilot_part1(&lines);
        }
        assert_eq!(value, 150);
    }

    #[test]
    fn sample_test_part2() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample.txt") {
            value = pilot_part2(&lines);
        }
        assert_eq!(value, 900);
    }
}
