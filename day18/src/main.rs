use std::env::args;
use std::error::Error;
use std::fmt::Display;
use std::fs::File;
use std::io::{self, BufRead};
use std::mem::replace;
use std::ops::{Add, AddAssign, Sub, SubAssign};
use std::path::Path;
use std::str::FromStr;
use std::time::Instant;

fn main() {
    let filename = args().nth(1).expect("program [filename]");
    if let Ok(lines) = read_lines(filename) {
        let sfs = load_snailfish(&lines);
        let now = Instant::now();
        let mag = part1(&sfs);
        let elapsed = now.elapsed();
        println!("{} ns", elapsed.as_nanos());
        println!("{}", mag);

        let now = Instant::now();
        let mag = part2(&sfs);
        let elapsed = now.elapsed();
        println!("{} ns", elapsed.as_nanos());
        println!("{}", mag);
    }
}

fn part1(sfs: &[Snailfish]) -> usize {
    let mut sf0 = sfs[0].clone();
    sf0.reduce();
    for sf in &sfs[1..] {
        sf0 += sf;
        sf0.reduce();
    }
    sf0.magnitude()
}

fn part2(sfs: &[Snailfish]) -> usize {
    let mut largest = 0;
    let sfs2 = <&[Snailfish]>::clone(&sfs);

    for (i1, sf1) in sfs.iter().enumerate() {
        for (i2, sf2) in sfs2.iter().enumerate() {
            if i1 != i2 {
                let mut sf = sf1.clone();
                sf += sf2;
                sf.reduce();
                let m = sf.magnitude();
                if m > largest {
                    largest = m;
                }
            }
        }
    }
    largest
}

#[derive(Debug, Clone, Copy, PartialEq)]
struct Depth(usize);

impl PartialEq<i32> for Depth {
    fn eq(&self, other: &i32) -> bool {
        self.0 == *other as usize
    }
}

impl Add for Depth {
    type Output = Self;
    fn add(self, rhs: Self) -> Self::Output {
        if usize::MAX - rhs.0 < self.0 {
            Depth(usize::MAX)
        } else {
            Depth(self.0 + rhs.0)
        }
    }
}

impl Add<i32> for Depth {
    type Output = Self;
    fn add(self, rhs: i32) -> Self::Output {
        if usize::MAX - (rhs as usize) < self.0 {
            Depth(usize::MAX)
        } else {
            Depth(self.0 + (rhs as usize))
        }
    }
}

impl Sub for Depth {
    type Output = Self;
    fn sub(self, rhs: Self) -> Self::Output {
        if rhs.0 > self.0 {
            Depth(0)
        } else {
            Depth(self.0 - rhs.0)
        }
    }
}

impl Sub<i32> for Depth {
    type Output = Self;
    fn sub(self, rhs: i32) -> Self::Output {
        if rhs as usize > self.0 {
            Depth(0)
        } else {
            Depth(self.0 - (rhs as usize))
        }
    }
}

impl AddAssign for Depth {
    fn add_assign(&mut self, rhs: Self) {
        if usize::MAX - rhs.0 < self.0 {
            (*self).0 = usize::MAX;
        } else {
            (*self).0 += rhs.0
        }
    }
}

impl AddAssign<i32> for Depth {
    fn add_assign(&mut self, rhs: i32) {
        *self += Depth(rhs as usize);
    }
}

impl SubAssign for Depth {
    fn sub_assign(&mut self, rhs: Self) {
        (*self).0 -= rhs.0
    }
}

impl SubAssign<i32> for Depth {
    fn sub_assign(&mut self, rhs: i32) {
        (*self).0 -= rhs as usize;
    }
}

#[derive(Debug, Clone, Copy)]
struct Value(usize);

#[derive(Debug, Clone, Copy)]
enum Node {
    OpenPair(Depth),
    ClosePair(Depth),
    Number((Depth, Value)),
}

impl Node {
    fn depth(&self) -> usize {
        match self {
            Node::OpenPair(dp) => dp.0,
            Node::ClosePair(dp) => dp.0,
            Node::Number((dp, _)) => dp.0,
        }
    }

    fn value(&self) -> Option<usize> {
        match self {
            Node::Number((_, v)) => Some((*v).0),
            _ => None,
        }
    }
}

#[derive(Debug)]
enum Elem {
    Pair([Box<Elem>; 2]),
    Number(usize),
}

impl Elem {
    fn magnitude(&self) -> usize {
        match self {
            Elem::Number(n) => *n,
            Elem::Pair(p) => 3 * p[0].magnitude() + 2 * p[1].magnitude(),
        }
    }
}

#[derive(Debug, Clone)]
struct Snailfish {
    inner: Vec<Node>,
}

impl Snailfish {
    fn reduce(&mut self) {
        let mut reducing = true;
        while reducing {
            reducing = false;
            let explode = search_explode(self);
            if let Some(ex) = explode {
                explode_starting_at(self, ex);
                eprintln!("after explode:\t{}", self);
                reducing = true;
                continue;
            }
            let split = search_split(self);
            if let Some(sp) = split {
                split_at(self, sp);
                eprintln!("after split:\t{}", self);
                reducing = true;
            }
        }
    }

    fn magnitude(&self) -> usize {
        let mut tree = Vec::new();
        let mut sf = (*self).clone();
        while !sf.inner.is_empty() {
            let node = sf.inner.remove(0);
            match node {
                Node::OpenPair(_) => {}
                Node::Number((_, val)) => tree.push(Elem::Number(val.0)),
                Node::ClosePair(_) => {
                    let el2 = tree.remove(tree.len() - 1);
                    let el1 = tree.remove(tree.len() - 1);
                    tree.push(Elem::Pair([Box::new(el1), Box::new(el2)]))
                }
            }
        }
        tree[0].magnitude()
    }
}

impl Add for Snailfish {
    type Output = Self;
    fn add(self, rhs: Self) -> Self::Output {
        let s = format!("[{},{}]", self, rhs);
        s.parse().unwrap()
    }
}

impl Add<&str> for Snailfish {
    type Output = Self;
    fn add(self, rhs: &str) -> Self::Output {
        let s = format!("[{},{}]", self, rhs);
        s.parse().unwrap()
    }
}

impl AddAssign for Snailfish {
    fn add_assign(&mut self, rhs: Self) {
        let s = format!("[{},{}]", self, rhs);
        let _drop = replace(self, s.parse().unwrap());
    }
}

impl AddAssign<&Snailfish> for Snailfish {
    fn add_assign(&mut self, rhs: &Snailfish) {
        let s = format!("[{},{}]", self, rhs);
        let _drop = replace(self, s.parse().unwrap());
    }
}

impl AddAssign<&str> for Snailfish {
    fn add_assign(&mut self, rhs: &str) {
        let s = format!("[{},{}]", self, rhs);
        let _drop = replace(self, s.parse().unwrap());
    }
}

impl FromStr for Snailfish {
    type Err = Box<dyn Error>;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        parse_snailfish(&mut s.to_owned())
    }
}

impl Display for Snailfish {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut s = String::new();
        let mut prev: Option<&Node> = None;
        for node in &self.inner {
            if let Some(n) = prev {
                match *n {
                    Node::Number(_) | Node::ClosePair(_) => match *node {
                        Node::Number(_) | Node::OpenPair(_) => s.push(','),
                        _ => {}
                    },
                    _ => {}
                }
            };
            match node {
                Node::OpenPair(_) => s.push('['),
                Node::ClosePair(_) => s.push(']'),
                Node::Number((_, v)) => s.push_str(&v.0.to_string()),
            }
            prev = Some(node);
        }
        write!(f, "{}", s)
    }
}

fn split_at(sf: &mut Snailfish, index: usize) {
    if let Some(&Node::Number((depth, value))) = sf.inner.get(index) {
        let floor = (value.0 as f64 / 2.).floor() as usize;
        let ceil = (value.0 as f64 / 2.).ceil() as usize;
        let depth = depth + 1;
        sf.inner.splice(
            index..=index,
            vec![
                Node::OpenPair(depth),
                Node::Number((depth, Value(floor))),
                Node::Number((depth, Value(ceil))),
                Node::ClosePair(depth),
            ],
        );
    }
}

fn explode_starting_at(sf: &mut Snailfish, index: usize) {
    let left = (&sf.inner[index]).value();
    let right = (&sf.inner[index + 1]).value();
    // first node before pair
    let mut i = index - 2;
    //try first number left
    while i != 0 {
        if let Node::Number((_, value)) = &mut sf.inner[i] {
            if let Some(lv) = left {
                (*value).0 += lv;
                break;
            }
        }
        i -= 1;
    }
    // first node after pair
    i = index + 3;
    // try first number right
    while i < sf.inner.len() {
        if let Node::Number((_, value)) = &mut sf.inner[i] {
            if let Some(rv) = right {
                (*value).0 += rv;
                break;
            }
        }
        i += 1;
    }
    let depth = (&sf.inner[index]).depth();
    sf.inner.splice(
        (index - 1)..=(index + 2),
        vec![Node::Number((Depth(depth - 1), Value(0)))],
    );
}

fn search_split(sf: &Snailfish) -> Option<usize> {
    sf.inner
        .iter()
        .enumerate()
        .filter(|(_, n)| match *n {
            Node::Number((_, v)) => v.0 >= 10,
            _ => false,
        })
        .map(|(i, _)| i)
        .next()
}

fn search_explode(sf: &Snailfish) -> Option<usize> {
    sf.inner
        .windows(2)
        .enumerate()
        .filter(|(_, nn)| {
            nn.iter()
                .all(|n| n.depth() == 5 && matches!(n, Node::Number(_)))
        })
        .map(|(i, _)| i)
        .next()
}

fn parse_snailfish(s: &mut String) -> Result<Snailfish, Box<dyn Error>> {
    let mut sf = Snailfish { inner: Vec::new() };
    let mut depth = Depth(0);
    let mut number = String::new();
    while !s.is_empty() {
        let c = s.remove(0);
        match c {
            '[' => {
                sf.inner.push(Node::OpenPair(depth));
                depth += 1;
            }
            ',' => {
                if !number.is_empty() {
                    let num = number.parse::<usize>()?;
                    sf.inner.push(Node::Number((depth, Value(num))));
                    number.clear();
                }
            }
            ']' => {
                if !number.is_empty() {
                    let num = number.parse::<usize>()?;
                    sf.inner.push(Node::Number((depth, Value(num))));
                    number.clear();
                }
                depth -= 1;
                sf.inner.push(Node::ClosePair(depth));
            }
            '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' => number.push(c),
            _ => return Err("snailfish parse error".into()),
        }
    }
    if depth != 0 {
        Err("unbalanced snailfish number".into())
    } else {
        Ok(sf)
    }
}

fn load_snailfish(lines: &[String]) -> Vec<Snailfish> {
    let mut sfs = Vec::new();
    for line in lines.iter() {
        sfs.push(line.parse().unwrap());
    }
    sfs
}

fn read_lines<P>(filename: P) -> io::Result<Vec<String>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sample1_magnitude() {
        let sf: Snailfish = "[1,2]".parse().unwrap();
        assert_eq!(sf.magnitude(), 7);
    }

    #[test]
    fn sample2_magnitude() {
        let sf: Snailfish = "[[9,1],[1,9]]".parse().unwrap();
        assert_eq!(sf.magnitude(), 129);
    }

    #[test]
    fn sample3_magnitude() {
        let sf: Snailfish = "[[1,2],[[3,4],5]]".parse().unwrap();
        assert_eq!(sf.magnitude(), 143);
    }

    #[test]
    fn sample4_magnitude() {
        let sf: Snailfish = "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]".parse().unwrap();
        assert_eq!(sf.magnitude(), 1384);
    }

    #[test]
    fn sample5_magnitude() {
        let sf: Snailfish = "[[[[1,1],[2,2]],[3,3]],[4,4]]".parse().unwrap();
        assert_eq!(sf.magnitude(), 445);
    }

    #[test]
    fn sample6_magnitude() {
        let sf: Snailfish = "[[[[3,0],[5,3]],[4,4]],[5,5]]".parse().unwrap();
        assert_eq!(sf.magnitude(), 791);
    }

    #[test]
    fn sample7_magnitude() {
        let sf: Snailfish = "[[[[5,0],[7,4]],[5,5]],[6,6]]".parse().unwrap();
        assert_eq!(sf.magnitude(), 1137);
    }

    #[test]
    fn sample8_magnitude() {
        let sf: Snailfish = "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]"
            .parse()
            .unwrap();
        assert_eq!(sf.magnitude(), 3488);
    }

    #[test]
    fn sample9_magnitude() {
        let sf: Snailfish = "[[[[6,6],[7,6]],[[7,7],[7,0]]],[[[7,7],[7,7]],[[7,8],[9,9]]]]"
            .parse()
            .unwrap();
        assert_eq!(sf.magnitude(), 4140);
    }

    #[test]
    fn sample0_add_reduce() {
        let mut value = String::new();
        if let Ok(lines) = read_lines("./sample0.txt") {
            let sfs = load_snailfish(&lines);
            let mut sf0 = sfs[0].clone();
            sf0.reduce();
            for sf in &sfs[1..] {
                sf0 += sf;
                sf0.reduce();
            }
            value.push_str(&sf0.to_string());
        }
        assert_eq!(&*value, "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]");
    }

    #[test]
    fn sample1_add_reduce() {
        let mut value = String::new();
        if let Ok(lines) = read_lines("./sample1.txt") {
            let sfs = load_snailfish(&lines);
            let mut sf0 = sfs[0].clone();
            for sf in &sfs[1..] {
                sf0 += sf;
                sf0.reduce();
            }
            value.push_str(&sf0.to_string());
        }
        assert_eq!(&*value, "[[[[1,1],[2,2]],[3,3]],[4,4]]");
    }

    #[test]
    fn sample2_add_reduce() {
        let mut value = String::new();
        if let Ok(lines) = read_lines("./sample2.txt") {
            let sfs = load_snailfish(&lines);
            let mut sf0 = sfs[0].clone();
            for sf in &sfs[1..] {
                sf0 += sf;
                sf0.reduce();
            }
            value.push_str(&sf0.to_string());
        }
        assert_eq!(&*value, "[[[[3,0],[5,3]],[4,4]],[5,5]]");
    }

    #[test]
    fn sample3_add_reduce() {
        let mut value = String::new();
        if let Ok(lines) = read_lines("./sample3.txt") {
            let sfs = load_snailfish(&lines);
            let mut sf0 = sfs[0].clone();
            for sf in &sfs[1..] {
                sf0 += sf;
                sf0.reduce();
            }
            value.push_str(&sf0.to_string());
        }
        assert_eq!(&*value, "[[[[5,0],[7,4]],[5,5]],[6,6]]");
    }

    #[test]
    fn sample4_add_reduce() {
        let mut value = String::new();
        if let Ok(lines) = read_lines("./sample4.txt") {
            let sfs = load_snailfish(&lines);
            let mut sf0 = sfs[0].clone();
            eprintln!("after initial:\t{}", sf0);
            sf0.reduce();
            for sf in &sfs[1..] {
                sf0 += sf;
                eprintln!("after addition:\t{}", sf0);
                sf0.reduce();
            }
            value.push_str(&sf0.to_string());
        }
        assert_eq!(
            &*value,
            "[[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]]"
        );
    }

    #[test]
    fn sample5_add_reduce() {
        let mut value = String::new();
        if let Ok(lines) = read_lines("./sample5.txt") {
            let sfs = load_snailfish(&lines);
            let mut sf0 = sfs[0].clone();
            eprintln!("after initial:\t{}", sf0);
            sf0.reduce();
            for sf in &sfs[1..] {
                sf0 += sf;
                eprintln!("after addition:\t{}", sf0);
                sf0.reduce();
            }
            value.push_str(&sf0.to_string());
        }
        assert_eq!(
            &*value,
            "[[[[6,6],[7,6]],[[7,7],[7,0]]],[[[7,7],[7,7]],[[7,8],[9,9]]]]"
        );
    }

    #[test]
    fn sample6_add_reduce() {
        let mut value = String::new();
        if let Ok(lines) = read_lines("./sample6.txt") {
            let sfs = load_snailfish(&lines);
            let mut sf0 = sfs[0].clone();
            eprintln!("after initial:\t{}", sf0);
            sf0.reduce();
            for sf in &sfs[1..] {
                sf0 += sf;
                eprintln!("after addition:\t{}", sf0);
                sf0.reduce();
            }
            value.push_str(&sf0.to_string());
        }
        assert_eq!(
            &*value,
            "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]"
        );
    }

    #[test]
    fn sample5_part1() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample5.txt") {
            let sfs = load_snailfish(&lines);
            value = part1(&sfs);
        }
        assert_eq!(value, 4140);
    }

    #[test]
    fn sample5_part2() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample5.txt") {
            let sfs = load_snailfish(&lines);
            value = part2(&sfs);
        }
        assert_eq!(value, 3993);
    }

    #[test]
    fn sample5_part2_part1() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample5_part2.txt") {
            let sfs = load_snailfish(&lines);
            value = part1(&sfs);
        }
        assert_eq!(value, 3993);
    }

    #[test]
    fn explode1() {
        let mut value = String::new();
        if let Ok(lines) = read_lines("./explode1.txt") {
            let sfs = load_snailfish(&lines);
            let mut sf0 = sfs[0].clone();
            eprintln!("after initial:\t{}", sf0);
            sf0.reduce();
            value.push_str(&sf0.to_string());
        }
        assert_eq!(&*value, "[[[[0,9],2],3],4]");
    }

    #[test]
    fn explode2() {
        let mut value = String::new();
        if let Ok(lines) = read_lines("./explode2.txt") {
            let sfs = load_snailfish(&lines);
            let mut sf0 = sfs[0].clone();
            eprintln!("after initial:\t{}", sf0);
            sf0.reduce();
            value.push_str(&sf0.to_string());
        }
        assert_eq!(&*value, "[7,[6,[5,[7,0]]]]");
    }

    #[test]
    fn explode3() {
        let mut value = String::new();
        if let Ok(lines) = read_lines("./explode3.txt") {
            let sfs = load_snailfish(&lines);
            let mut sf0 = sfs[0].clone();
            eprintln!("after initial:\t{}", sf0);
            sf0.reduce();
            value.push_str(&sf0.to_string());
        }
        assert_eq!(&*value, "[[6,[5,[7,0]]],3]");
    }

    #[test]
    fn explode4() {
        let mut value = String::new();
        if let Ok(lines) = read_lines("./explode4.txt") {
            let sfs = load_snailfish(&lines);
            let mut sf0 = sfs[0].clone();
            eprintln!("after initial:\t{}", sf0);
            sf0.reduce();
            value.push_str(&sf0.to_string());
        }
        assert_eq!(&*value, "[[3,[2,[8,0]]],[9,[5,[7,0]]]]");
    }
}
