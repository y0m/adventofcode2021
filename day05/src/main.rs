#![allow(dead_code)]
use std::env::args;
use std::fs::File;
use std::io::{self, BufRead, Result};
use std::path::Path;
use std::time::Instant;

fn main() {
    let filename = args().nth(1).expect("program [filename]");
    if let Ok(lines) = read_lines(filename) {
        let (vectors, xmax, ymax) = load_points(lines);
        let now = Instant::now();
        let (res, _) = overlap_part1(&vectors, xmax, ymax);
        let elapsed = now.elapsed();
        eprintln!("{} ns", elapsed.as_nanos());
        println!("{}", res);
        let now = Instant::now();
        let (res, _) = overlap_part2(&vectors, xmax, ymax);
        let elapsed = now.elapsed();
        eprintln!("{} ns", elapsed.as_nanos());
        println!("{}", res);
    }
}

fn overlap_part1(vectors: &[Vector], xmax: i64, ymax: i64) -> (usize, Vec<usize>) {
    let mut points = vec![0usize; (xmax * ymax) as usize];
    vectors.iter().filter(|&x| x.x1 == x.x2).for_each(|p| {
        let mut y1 = (p.x1 + p.y1 * xmax) as usize;
        let y2 = (p.x1 + p.y2 * xmax) as usize;
        while y1 <= y2 {
            points[y1] += 1;
            y1 += xmax as usize;
        }
    });
    vectors.iter().filter(|&y| y.y1 == y.y2).for_each(|p| {
        let x1 = (p.x1 + p.y1 * xmax) as usize;
        let x2 = (p.x2 + p.y1 * xmax) as usize;
        for pp in &mut points[x1..=x2] {
            *pp += 1
        }
    });
    (points.iter().filter(|&&o| o >= 2).count(), points)
}

fn overlap_part2(vectors: &[Vector], xmax: i64, ymax: i64) -> (usize, Vec<usize>) {
    let mut points = vec![0usize; (xmax * ymax) as usize];
    vectors.iter().filter(|&x| x.x1 == x.x2).for_each(|p| {
        let mut i1 = (p.x1 + p.y1 * xmax) as usize;
        let i2 = (p.x1 + p.y2 * xmax) as usize;
        while i1 <= i2 {
            points[i1] += 1;
            i1 += xmax as usize;
        }
    });
    vectors.iter().filter(|&y| y.y1 == y.y2).for_each(|p| {
        let i1 = (p.x1 + p.y1 * xmax) as usize;
        let i2 = (p.x2 + p.y1 * xmax) as usize;
        for pp in &mut points[i1..=i2] {
            *pp += 1
        }
    });
    vectors
        .iter()
        .filter(|&d| (d.x2 - d.x1).abs() == (d.y2 - d.y1).abs())
        .for_each(|v| {
            let mut i1 = (v.x1 + v.y1 * xmax) as usize;
            let mut i2 = (v.x2 + v.y2 * xmax) as usize;
            if i1 > i2 {
                std::mem::swap(&mut i1, &mut i2);
            }
            let slope = (v.y2 - v.y1) as f64 / (v.x2 - v.x1) as f64;
            let step = if slope < 0. {
                (xmax - 1) as usize
            } else {
                (xmax + 1) as usize
            };
            while i1 <= i2 {
                points[i1] += 1;
                i1 += step;
            }
        });
    (points.iter().filter(|&&o| o >= 2).count(), points)
}

fn load_points(lines: Vec<String>) -> (Vec<Vector>, i64, i64) {
    let mut vectors: Vec<Vector> = Vec::new();
    let mut xmax = 0;
    let mut ymax = 0;
    for line in lines {
        let mut points = line.split(" -> ");
        let mut p1 = points.next().unwrap().split(',');
        let mut p2 = points.next().unwrap().split(',');
        let mut p = Vector {
            x1: p1.next().unwrap().parse::<i64>().unwrap(),
            y1: p1.next().unwrap().parse::<i64>().unwrap(),
            x2: p2.next().unwrap().parse::<i64>().unwrap(),
            y2: p2.next().unwrap().parse::<i64>().unwrap(),
        };
        if p.x1 > xmax {
            xmax = p.x1
        }
        if p.x2 > xmax {
            xmax = p.x2
        }
        if p.y1 > ymax {
            ymax = p.y1
        }
        if p.y2 > ymax {
            ymax = p.y2
        }
        if p.x1 == p.x2 && p.y1 > p.y2 {
            std::mem::swap(&mut p.y1, &mut p.y2);
        }
        if p.y1 == p.y2 && p.x1 > p.x2 {
            std::mem::swap(&mut p.x1, &mut p.x2);
        }
        vectors.push(p);
    }
    (vectors, xmax + 1, ymax + 1)
}

fn print_diagram(points: &[usize], xmax: usize, ymax: usize) {
    for (i, &o) in points.iter().enumerate() {
        if i % ymax == 0 && i / xmax != 0 {
            println!();
        }
        match o {
            0 => print!("."),
            x => print!("{}", x),
        }
    }
    println!();
}

#[derive(Debug)]
struct Vector {
    x1: i64,
    y1: i64,
    x2: i64,
    y2: i64,
}

fn read_lines<P>(filename: P) -> Result<Vec<String>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sample_test_part1() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample.txt") {
            let (vectors, xmax, ymax) = load_points(lines);
            let (res, points) = overlap_part1(&vectors, xmax, ymax);
            print_diagram(&points, xmax as usize, ymax as usize);
            value = res;
        }
        assert_eq!(value, 5);
    }

    #[test]
    fn sample_test_part2() {
        let mut value = 0;
        if let Ok(lines) = read_lines("./sample.txt") {
            let (vectors, xmax, ymax) = load_points(lines);
            let (res, points) = overlap_part2(&vectors, xmax, ymax);
            print_diagram(&points, xmax as usize, ymax as usize);
            value = res;
        }
        assert_eq!(value, 12);
    }
}
